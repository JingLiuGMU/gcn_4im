python ./code/gcn/main.py \
--max_step 5000 \
--learning_rate 0.005 \
--ch_num 4 \
--keep_r 0.2 \
--weight_decay 5e-4 \
--k 2 \
--gcl_linear=True \
--full_map=True \
--vertex_feat=True \
--filename ./data/CVE/size_4_stride_2/CVE_input_lt_prob_span_ \
--window_size 4 \
--stride 2 \
--spans 4 \
--k_fold=False \
--aggregator mean \
--gpuid "1" \
--reg_model multi_logit \
--n_slice 1 \
--dataset CVE \
--exp_name {dataset}_w{window_size}_s{stride}_lr{learning_rate}_keep{keep_r}_ch{ch_num}_l2_{weight_decay}_k{k}_gcl{layer_num}_gcl_linear{gcl_linear}_full_m_{full_map}_vfeat_{vertex_feat}_{reg_model}_{aggregator}_b{use_batch}_bsize{batch_size}_self{self_loop} \
-logdir ./data/logdir/ \
--modeldir ./data/modeldir/lt/ \
--fc_layer_num 1 \
--layer_num 2
