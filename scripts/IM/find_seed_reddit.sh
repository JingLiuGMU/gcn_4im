#!/usr/bin/env bash
method=IC

for k in 5 10
do

for graph in ours true 0 1 2 3
do

for prob in bern jaccard lt
do

python3 code/baseline/IMP-master/IMP_original.py -i data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/graph_${graph}_${prob}.txt -k ${k} -m ${method} -o data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/seed-${k}_g-${graph}_${method}_${prob}.txt

done

done
done
