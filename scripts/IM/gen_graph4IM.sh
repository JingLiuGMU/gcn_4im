#!/usr/bin/env bash

python3 code/data_prep/gen_graph_4IM.py --dir data/temporal_network/ --dataset soc-redditHyperlinks-body --stride 8 --span 4 --window_size 8 --prob_name bern