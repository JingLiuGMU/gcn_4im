#!/usr/bin/env bash

method=IC

for k in 5 10
do

for graph in ours true robust 0 1 2 3
do

for prob in bern jaccard lt
do

python3 code/baseline/IMP-master/ISE.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/graph_true_${prob}.txt -k ${k} -m ${method} -s /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/seed-${k}_g-${graph}_${method}_${prob}.txt -o /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/result_IM4TrueG.csv -p ${prob} -g ${graph}

done

done
done