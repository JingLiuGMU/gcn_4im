#!/usr/bin/env bash

method=IC

python3 code/baseline/IMP-master/IMP.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ -g 4 -sf _bern.txt -pn bern -k 5 -m ${method} -b 0 -im newgr -inf 11.66405 10.91802 10.704204 10.536188

python3 code/baseline/IMP-master/IMP.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ -g 4 -sf _bern.txt -pn bern -k 10 -m ${method} -b 0 -im newgr -inf 22.7039 20.783384 20.857443 20.424189


python3 code/baseline/IMP-master/IMP.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ -g 4 -sf _jaccard.txt -pn jaccard -k 5 -m ${method} -b 0 -im newgr -inf 9.754184 9.144508 9.133549 9.023608

python3 code/baseline/IMP-master/IMP.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ -g 4 -sf _jaccard.txt -pn jaccard -k 10 -m ${method} -b 0 -im newgr -inf 17.017552 16.854815 17.7686 16.658496


python3 code/baseline/IMP-master/IMP.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ -g 4 -sf _lt.txt -pn lt -k 5 -m ${method} -b 0 -im newgr -inf 96.372 91.74 87.636 89.22

python3 code/baseline/IMP-master/IMP.py -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ -g 4 -sf _lt.txt -pn lt -k 10 -m ${method} -b 0 -im newgr -inf 127.2 126.18 120.312 122.076
