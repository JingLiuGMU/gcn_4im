import numpy as np
from scipy import sparse
import time


def get_indice_graph(adj, mask, size, keep_r=1.0):
    indices = mask.nonzero()[0]
    if keep_r < 1.0:
        indices = np.random.choice(indices, int(indices.size*keep_r), False)
    pre_indices = set()
    indices = set(indices)
    while len(indices) < size:
        new_add = indices - pre_indices
        if not new_add:
            break
        pre_indices = indices
        candidates = get_candidates(adj, new_add) - indices
        if len(candidates) > size - len(indices):
            candidates = set(np.random.choice(list(candidates), size-len(indices), False))
        indices.update(candidates)
    print('indices size:-------------->', len(indices))
    return sorted(indices)


def get_indice_connected_subgraph(adj_v, adj_e, size, center_num=1):
    nonzero_indices = adj_v.nonzero()[0]
    #print('nonzero_indices length: {}'.format(len(nonzero_indices)))
    pre_indices = set()
    indices = set(np.random.choice(nonzero_indices, center_num, False))
    while len(indices) < size:
        new_add = indices - pre_indices
        if not new_add:
            break
        pre_indices = indices.copy()
        candidates = get_candidates(adj_v, new_add) - indices
        if len(candidates) > size - len(indices):
            np.random.seed(int(time.time()))
            candidates = set(np.random.choice(list(candidates), size-len(indices), False))
        indices.update(candidates)

    v_index = sorted(indices)
    sample_adj_e = adj_e[:, v_index]
    e_index = sorted(set(np.where(sample_adj_e.sum(axis=1) == 0)[0]) -
                     set(np.where(~sample_adj_e.any(axis=1))[0]))  # find the closed edge by removing all-zero rows

    assert len(e_index) > 0
    print('indices size:-------------->', len(indices))
    return v_index, e_index


def save_to_file(data):
    import csv
    writer = csv.writer(open('sampled_index.csv', 'a'))
    writer.writerow(data)


def get_sampled_index(adj_v, adj_e, size, center_num=1):
    n = adj_v.shape[0]
    nonzero_indices = adj_v.nonzero()[0]
    pre_indices = set()
    incld_indices = set()
    indices = set(np.random.choice(nonzero_indices, center_num, False))
    while len(indices) < size:
        if len(pre_indices) != len(indices):
            new_add = indices - pre_indices
            pre_indices = indices.copy()
            candidates = get_candidates(adj_v, new_add) - indices
        else:
            candidates = random_num(n, center_num, indices)
        sample_size = min(len(candidates), size - len(indices))
        if not sample_size:
            if len(indices) < size:
                # sampled one connected subgraph
                incld_indices.update(indices)
                candidates = random_num(n, center_num, indices)
            else:
                break
        if len(candidates) > size - len(indices):
            candidates = set(np.random.choice(list(candidates), size - len(indices), False))
        else:
            incld_indices.update(indices)
        indices.update(candidates)

    v_index = sorted(indices)
    sample_adj_e = adj_e[:, v_index]
    e_index = sorted(set(np.where(sample_adj_e.sum(axis=1) == 0)[0]) -
                     set(np.where(~sample_adj_e.any(axis=1))[0]))  # find the closed edge by removing all-zero rows

    sample_incld_adj_e = adj_e[:, sorted(incld_indices)]
    e_incld_index = sorted(set(np.where(sample_incld_adj_e.sum(axis=1) == 0)[0]) -
                     set(np.where(~sample_incld_adj_e.any(axis=1))[0]))

    mask = np.in1d(np.asarray(e_index),e_incld_index).astype(int)
    return v_index, e_index, mask


def get_candidates(adj, new_add):
    return set(adj[:, sorted(new_add)].sum(axis=1).nonzero()[0])


def random_num(n, num, indices):
    cans = set(np.arange(n)) - indices
    num = min(num, len(cans))
    if len(cans) == 0:
        return set()
    new_add = set(np.random.choice(list(cans), num, replace=False))
    return new_add


def gen_adj_e(adj_v):
    adj_v_sparse = sparse.coo_matrix(adj_v)
    edge_no = adj_v_sparse.count_nonzero()
    data = np.hstack((-np.ones(edge_no), np.ones(edge_no)))
    row = np.hstack((np.arange(edge_no), np.arange(edge_no)))
    col = np.hstack((adj_v_sparse.row, adj_v_sparse.col))
    adj_e = sparse.coo_matrix((data, (row, col)),
                              shape=(edge_no, adj_v.shape[0]), dtype=int)  # Ne x Nv
    return adj_e.toarray()


if __name__ == '__main__':
    inputs = np.load('data/sentiment_incld/ransomware/size_5_stride_2/ransomware_input_bern_prob_span_0.npz')
    adj_v = inputs['Av']
    adj_ev = inputs['Ae']
    result = get_sampled_index(adj_v, adj_ev, 600, 60)
    for i in range(10):
        result_i = get_sampled_index(adj_v, adj_ev, 600, 60)

        diff = set(result[0]) - set(result_i[0])
        print(len(diff))
        result = result_i