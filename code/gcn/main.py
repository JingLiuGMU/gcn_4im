import os
import argparse
import tensorflow as tf
from kdd_network import GraphNet


####Delete all flags before declare#####
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)


del_all_flags(tf.flags.FLAGS)

flags = tf.app.flags
flags.DEFINE_integer('max_step', 10000, '# of step for training')
flags.DEFINE_integer('summary_interval', 10, '# of step to save summary')
flags.DEFINE_float('learning_rate', 0.0001, 'learning rate')
flags.DEFINE_boolean('is_train', True, 'is train')
flags.DEFINE_string('filename', '../../data/CVE/CVE_input_bern_prob_span_', 'input file path and name')
flags.DEFINE_integer('spans', 4, 'spans numbers, the last span treated as test set')
flags.DEFINE_string('prob_name', 'BT', 'type of probability definition')
flags.DEFINE_string('gpuid', '1', 'specify gpuid')
flags.DEFINE_integer('window_size', 4, 'window size')
flags.DEFINE_integer('stride', 2, 'stride')
flags.DEFINE_string('dataset', 'CVE', 'dataset')
# Debug
flags.DEFINE_string('logdir', '../../data/logdir/', 'Log dir')
flags.DEFINE_string('modeldir', '../../data/modeldir/', 'Model dir')
flags.DEFINE_string('model_name', 'model', 'Model file name')
flags.DEFINE_integer('reload_step', 0, 'Reload step to continue training')
flags.DEFINE_integer('print_step', 50, 'step to print training loss')
flags.DEFINE_integer('test_step', 500, 'Test or predict model at this step when training')
flags.DEFINE_string('what', 'train', 'train model or load model for predict')
flags.DEFINE_string('load_step', None, 'load step to test model')
# network architecture
flags.DEFINE_integer('ch_num', 4, 'channel number')
flags.DEFINE_integer('layer_num', 2, 'block number')
flags.DEFINE_integer('fc_layer_num', 1, 'fc layer after LGL')
flags.DEFINE_float('adj_keep_r', 1, 'dropout keep rate')
flags.DEFINE_float('keep_r', 0.8, 'dropout keep rate')
flags.DEFINE_float('weight_decay', 5e-4, 'Weight for L2 loss on embedding matrix.')
flags.DEFINE_integer('k', 2, 'top k')
flags.DEFINE_string('first_conv', 'graph_embedding', 'graph_embedding, chan_conv')
flags.DEFINE_string('second_conv', 'graph_conv', 'graph_conv, simple_conv')
flags.DEFINE_boolean('use_batch', False, 'use batch training')
flags.DEFINE_integer('batch_size', 2500, 'batch size number')
flags.DEFINE_integer('center_num', 1500, 'start center number')
flags.DEFINE_boolean('self_loop', False, 'aggregation including the center vertex')
flags.DEFINE_boolean('gcl', True, 'whether to use gcl')
flags.DEFINE_boolean('gcl_linear', False, 'whether to use linear layer after gcl')
flags.DEFINE_boolean('full_map', False, 'whether to add a full map after gcl')
flags.DEFINE_boolean('vertex_feat', True, 'whether to use vertex feature for prediction')
flags.DEFINE_boolean('k_fold', True, 'whether to use k-fold, leave one out is implemented')
flags.DEFINE_string('aggregator', 'mean', 'aggregator options')
flags.DEFINE_string('reg_model', 'sigmoid', 'regression model: sigmoid/beta regression/tanh/multi_logit')
flags.DEFINE_integer('n_slice', 2, 'number of slices on adj_m/feat_m')
flags.DEFINE_integer('max_degree', 3, 'max degree')
flags.DEFINE_string('exp_name', '', 'experiment name: e.g., {model}_{netG}_size{loadSize}')
flags.DEFINE_string('metric_name', 'test_metrics_not_norm_feat_jt', 'metric_name to save test metrics')
flags.DEFINE_boolean('fixed_sub', False, 'fixed size subgraph')
flags.DEFINE_integer('factor', 100, 'factor for loss function')
flags.DEFINE_boolean('remove_zero', False, 'whether to remove zero for MR')
flags.DEFINE_boolean('mul', False, 'whether to multiply MR prediction with sigmoid')
# flags.DEFINE_boolean('classifier', False, 'whether to train another classifier network')
# fix bug of flags
flags.FLAGS.__dict__['__parsed'] = False


def main(_):
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    model = GraphNet(tf.Session(config=config), flags.FLAGS)
    import time
    if flags.FLAGS.what == 'train':
        start_time = time.time()
        model.train()
        print("--- %.4f hours ---" % ((time.time() - start_time) / 3600))
    elif flags.FLAGS.what == 'test':
        model.test()
    else:
        raise NotImplementedError('{} mode does not support'.format(flags.FLAGS.what))


if __name__ == '__main__':

    if flags.FLAGS.gpuid:
    # configure which gpu or cpu to use
        os.environ['CUDA_VISIBLE_DEVICES'] = flags.FLAGS.gpuid
    tf.app.run()

