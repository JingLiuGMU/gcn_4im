import os
import sys
import json
import numpy as np
import pickle as pkl
import networkx as nx
import scipy.sparse as sp
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error


def load_data(dataset_str, batches):
    return load_small_data(dataset_str, batches)


def load_small_data(dataset_str, batches):
    """Load data."""
    #adjs_v_train = []
    #adjs_e_train = []
    #edge_connectivity_train = []
    feats_v_train = []
    feats_e_train = []
    labels_train = []
    #adjs_v_test = []
    #adjs_e_test = []
    #edge_connectivity_test = []
    feats_v_test = []
    feats_e_test = []
    labels_test = []
    filename = dataset_str + '0.npz'
    inputs = np.load(filename)
    adjs_v = inputs['Av']
    adjs_e = inputs['Ae']
    edge_connectivity = inputs['edge_connectivity']
    max_degree = np.max(inputs['Av'].sum(axis=0))
    mean_degree = np.mean(inputs['Av'].sum(axis=0))
    print('max degree: {}, mean: {}'.format(max_degree, mean_degree))
    for i in range(batches):
        filename = dataset_str + str(i) + '.npz'
        inputs = np.load(filename)
        if i < (batches - 1):
            #feats_v_train.append(inputs['fv'][:, -2:])
            feats_v_train.append(inputs['fv'][:, [-1]])
            # feats_v_train.append(inputs['fv'])
            feats_e_train.append(inputs['fe'])
            labels_train.append(inputs['label'])
        elif i == (batches - 1):
            feats_v_test.append(inputs['fv'][:, [-1]])
            #feats_v_test.append(inputs['fv'][:, -2:])
            # feats_v_test.append(inputs['fv'])
            feats_e_test.append(inputs['fe'])
            labels_test.append(inputs['label'])
    return adjs_v, adjs_e, edge_connectivity, feats_v_train, feats_e_train, labels_train, \
           feats_v_test, feats_e_test, labels_test


def normalize_adj(adj):
    """Symmetrically normalize adjacency matrix."""
    adj = sp.coo_matrix(adj)
    rowsum = np.array(adj.sum(1))
    d_inv_sqrt = np.power(rowsum, -1).flatten()
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = sp.csr_matrix(d_inv_sqrt)
    return d_mat_inv_sqrt.T.multiply(adj).tocoo()


def preprocess_adj(adj, self_loop=False, norm=True, sparse=False):
    """Preprocessing of adjacency matrix for simple GCN model and conversion to tuple representation."""

    if self_loop:
        # adj[i] = adj[i] + sp.eye(adj[i].shape[0], adj[i].shape[1])
        np.fill_diagonal(adj, 1)
    if norm:
        adj = normalize_adj(adj).todense()
    if sparse:
        adj = sparse_to_tuple(adj)
        # adj[i] = adj[i].todense()
    return adj


def gen_in_out_e_adj(adj, norm=True):

    adj_incoming_e = np.copy(adj)
    adj_incoming_e[adj_incoming_e == -1] = 0
    incoming_adj = adj_incoming_e
    adj_outgoing_e = np.copy(adj)
    adj_outgoing_e[adj_outgoing_e == 1] = 0
    outgoing_adj = np.abs(adj_outgoing_e)
    # incoming_adj = preprocess_adj(incoming_adj)
    # outgoing_adj = preprocess_adj(outgoing_adj)
    return incoming_adj, outgoing_adj


def sparse_to_tuple(sparse_mx):
    """Convert sparse matrix to tuple representation."""

    def to_tuple(mx):
        if not sp.isspmatrix_coo(mx):
            mx = mx.tocoo()
        coords = np.vstack((mx.row, mx.col)).transpose()
        values = mx.data
        shape = mx.shape
        return coords, values, shape

    if isinstance(sparse_mx, list):
        for i in range(len(sparse_mx)):
            sparse_mx[i] = to_tuple(sparse_mx[i])
    else:
        sparse_mx = to_tuple(sparse_mx)

    return sparse_mx


def preprocess_features(features, sparse=True):
    """Row-normalize feature matrix and convert to tuple representation"""
    for i in range(len(features)):
        rowsum = np.array(features[i].sum(1))
        # import ipdb; ipdb.set_trace()
        try:
            r_inv = np.power(rowsum, -1).flatten()
            r_inv[np.isinf(r_inv)] = 0.
            r_mat_inv = sp.csr_matrix(r_inv)
            features[i] = r_mat_inv.T.multiply(features[i]).todense()
        except Exception as e:
            print(e)

        if sparse:
            features[i] = sparse_to_tuple(features[i])

    return features


def preprocess_features_col(features, sparse=False):
    """Row-normalize feature matrix and convert to tuple representation"""
    for i in range(len(features)):
        if features[i].shape[1] == 1:
            features[i] = normalize_1D_feat(features[i])
        else:

            colsum = np.array(features[i].sum(0))
        # import ipdb; ipdb.set_trace()
            try:
                r_inv = np.power(colsum, -1).flatten()
                r_inv[np.isinf(r_inv)] = 0.
                r_mat_inv = sp.csr_matrix(r_inv)
                features[i] = r_mat_inv.T.multiply(features[i].T).T.todense()
            except Exception as e:
                print(e)

        if sparse:
            features[i] = sparse_to_tuple(features[i])

    return features


def normalize_1D_feat(feature):
    colsum = np.array(feature.sum(0))
    # import ipdb; ipdb.set_trace()
    try:
        r_inv = np.power(colsum, -1).flatten()
        r_inv[np.isinf(r_inv)] = 0.
        feature = np.multiply(feature, r_inv)
    except Exception as e:
        print(e)
    return feature


# generate partition mask for each adjs
def get_partition_mask(adjs, n_slice, adj_e=None):
    # if adj_e is not None:
    #     assert len(adjs) == len(adj_e)
    # partitions = []
    nrow = adjs.shape[0]
    if adj_e is not None:
        nrow += adj_e.shape[0]
    partition = gen_partition(nrow, n_slice)
    #partitions.append(partition)
    return partition


def gen_partition(nrow, n_slice):
    slice_size = int(nrow / n_slice)
    partition = np.zeros(slice_size, dtype=int)

    for i in range(1, n_slice):
        if i == n_slice - 1:
            slice_size = nrow - i * slice_size
        partition = np.concatenate([partition, i * np.ones(slice_size, dtype=int)])
    return partition


def cal_evl_metric(pred, label, remove=False):
    if remove:
        pred = pred[np.where(pred > 0.)]
        label = label[np.where(pred > 0.)]
    r2 = r2_score(label, pred)
    mae = mean_absolute_error(label, pred)
    mse = mean_squared_error(label, pred)
    mape = mean_absolute_percentage_error(label, pred)
    return r2, mae, mse, mape


def mean_absolute_percentage_error(y_true, y_pred):
    predi = y_pred[np.where(y_true > 1e-5)]
    labeli = y_true[np.where(y_true > 1e-5)]
    return np.mean(np.abs(labeli - predi) / labeli)




