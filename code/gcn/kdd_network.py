import os
import time
import random
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import tensorflow as tf
import kdd_ops
import kdd_utils
from kdd_utils import load_data, preprocess_features, preprocess_adj, gen_in_out_e_adj, \
    preprocess_features_col, get_partition_mask
from batch_utils import get_sampled_index, get_indice_connected_subgraph
import csv


class GraphNet(object):

    def __init__(self, sess, conf):
        """
        sess: tf.Session()
        conf: parameters configuration
        """
        self.sess = sess
        # print(self.conf.max_step)
        # process opt.suffix
        if conf.exp_name and not conf.k_fold:
            exp_name = (conf.exp_name.format(**(conf.flag_values_dict()))) if conf.exp_name != '' else ''
            exp_name = exp_name.replace(', ', '+')
            exp_name = exp_name.replace('[', '')
            exp_name = exp_name.replace(']', '')
            conf.modeldir = conf.modeldir + exp_name + '/'
            conf.logdir = conf.logdir + exp_name + '/'

        if not conf.k_fold:  # not k_fold cross validation, create logdir, modeldir
            if not os.path.exists(conf.modeldir):
                os.makedirs(conf.modeldir)
            if not os.path.exists(conf.logdir):
                os.makedirs(conf.logdir)
        self.conf = conf
        self.process_data()
        self.configure_networks()
        self.train_summary = self.config_summary('train')
        self.valid_summary = self.config_summary('valid')
        self.test_summary = self.config_summary('test')

    def inference(self):
        """
        Forward propagation to get the final predictions
                1. Graph embedding layer
                2. Graph convolution layer
                3. Fully connected layer
                4. Output layer
        """
        # graph embedding layer
        num_feat = self.vertex_features.shape[-1].value + self.edge_features.shape[-1].value
        out_v, out_e = getattr(kdd_ops, self.conf.first_conv)(
            self.normed_adj_ev_matrix, self.vertex_features,
            self.edge_features, 2 * self.conf.ch_num,
            self.conf.keep_r, self.is_train, self.conf.use_batch, 'embedding', self.conf.n_slice,
            self.partitions_e, self.partitions_v,
            act_fn=None, aggregator=self.conf.aggregator)
        # graph conv layer
        for layer_index in range(self.conf.layer_num):
            cur_out_v, cur_out_e = getattr(kdd_ops, self.conf.second_conv)(
                self.normed_adj_v_matrix, self.incoming_e, self.outgoing_e, out_v, out_e, self.adj_edge,
                num_feat, self.conf.adj_keep_r,
                self.conf.keep_r, self.is_train, self.conf.use_batch, 'conv_l%s' % (layer_index + 1),
                self.conf.n_slice, self.partitions_ev,
                act_fn=None, k=self.conf.k, aggregator=self.conf.aggregator)
            out_v = tf.concat([out_v, cur_out_v], axis=1, name='concat_vertex_l%s' % layer_index)
            out_e = tf.concat([out_e, cur_out_e], axis=1, name='concat_edge_l%s' % layer_index)
            # linear lay after aggregation
            if self.conf.gcl_linear:
                out_e = kdd_ops.linear(out_e, num_feat,
                                       self.conf.keep_r, self.is_train, 'gcl_linear_edge_l%s' % layer_index,
                                       act_fn=None)
                out_v = kdd_ops.linear(out_v, num_feat,
                                       self.conf.keep_r, self.is_train, 'gcl_linear_vertex_l%s' % layer_index,
                                       act_fn=None)
        out_e = tf.concat([self.edge_features, out_e], axis=1, name='concat_input_edge_feat')
        # out_e = self.edge_features
        if self.conf.vertex_feat:
            # out_v = tf.concat([self.vertex_features, out_v], axis=1, name='concat_input_vertex_feat')
            incoming_v = tf.matmul(self.incoming_e, self.vertex_features, name='incoming_v_feat')
            outgoing_v = tf.matmul(self.outgoing_e, self.vertex_features, name='outgoing_v_feat')
            out_e = tf.concat([outgoing_v, incoming_v, out_e], axis=1, name='concat_vertex_edge_feat')
        self.concated_feat = tf.concat([out_e, tf.expand_dims(self.labels, axis=-1)], axis=1)

        if self.conf.full_map:
            out_e = kdd_ops.linear(
                out_e, out_e.shape[-1].value,
                1.0, self.is_train, 'linear_full_map', act_fn=tf.nn.relu)

        # multiple fc layers
        for layer_index in range(self.conf.fc_layer_num):
            num_out = out_e.shape[-1].value
            num_out = num_out // 2 + 1
            out_e = kdd_ops.linear(
                out_e, num_out,
                1.0, self.is_train, 'linear_output_l%s' % layer_index, act_fn=tf.nn.relu)
        # output layer
        if self.conf.reg_model == 'sigmoid':
            out_e = kdd_ops.linear(
                out_e, 1,
                1.0, self.is_train, 'linear_output', act_fn=tf.nn.sigmoid)
        elif self.conf.reg_model == 'tanh':
            out_e = kdd_ops.linear(
                out_e, 1,
                1.0, self.is_train, 'linear_output', act_fn=tf.nn.tanh)
            out_e = tf.abs(out_e)
        elif self.conf.reg_model == 'multi_logit':
            out_e = kdd_ops.linear(
                out_e, 1,
                1.0, self.is_train, 'linear_output', act_fn=None)  # w x he get Ne x 1
            out_e = kdd_ops.multi_logit_pred(self.incoming_e, out_e)

        else:
            raise NotImplementedError('{} regression model Not support!'.format(self.conf.reg_model))
        return out_e

    def get_optimizer(self, lr):
        # return tf.contrib.opt.NadamOptimizer(lr)
        return tf.train.AdamOptimizer(lr)

    def process_data(self):
        '''
        Organize the data into training, testing datasets
        For each dataset:
                        vertex, edge&vertex, and edge&edge are considered
                        please refer to kdd_utils for detailed function'''
        print('process input data...')
        data = load_data(self.conf.filename, self.conf.spans)
        # training dataset
        adj_v, adj_ev = data[:2]
        print('no. of vertex: {}, no. of edge: {}'.format(adj_v.shape[0], adj_ev.shape[0]))
        # self.adj_v_train, self.adj_e_train = adj_v, adj_e
        self.normed_adj_v = preprocess_adj(adj_v, self.conf.self_loop)
        self.normed_adj_ev = adj_ev  # preprocess_adj(adj_e, False)
        self.adj_incoming_e, self.adj_outgoing_e = gen_in_out_e_adj(adj_ev)

        edge_connect = data[2]
        self.edge_connect = preprocess_adj(edge_connect, self.conf.self_loop)
        feas = data[3:5]
        self.feat_v_train = feas[0]  # preprocess_features_col(feas[0], False)
        self.feat_e_train = feas[1]  # preprocess_features_col(feas[1], False)
        self.labels_train = data[5]
        self.parts_v_mask = get_partition_mask(adj_v, self.conf.n_slice)
        self.parts_e_mask = get_partition_mask(adj_ev, self.conf.n_slice)
        self.parts_ev_mask = get_partition_mask(adj_v, self.conf.n_slice, adj_ev)

        feas = data[6:8]
        self.feat_v_test = feas[0]  # preprocess_features_col(feas[0], False)
        self.feat_e_test = feas[1]  # preprocess_features_col(feas[1], False)
        self.labels_test = data[8]

    def configure_networks(self):
        print('configure network...')
        self.build_network()
        self.cal_loss()
        optimizer = self.get_optimizer(self.conf.learning_rate)
        self.train_op = optimizer.minimize(self.loss_op, name='train_op')
        self.seed = int(time.time())
        tf.set_random_seed(self.seed)
        self.sess.run(tf.global_variables_initializer())
        trainable_vars = tf.trainable_variables()
        self.saver = tf.train.Saver(var_list=trainable_vars, max_to_keep=0)
        if self.conf.is_train:
            self.writer = tf.summary.FileWriter(self.conf.logdir, self.sess.graph)
        self.print_params_num()

    def build_network(self):
        # self.adj_edge_vertex = tf.placeholder(tf.float32, [None, None], name='adj_edge_matrix')  # Ne x Nv
        # self.adj_vertex = tf.placeholder(tf.float32, [None, None], name='adj_vertex_matrix')  # Nv x Nv
        self.adj_edge = tf.placeholder(tf.float32, [None, None])  # Ne x Ne
        self.incoming_e = tf.placeholder(tf.float32, [None, None])  # Ne x Nv
        self.outgoing_e = tf.placeholder(tf.float32, [None, None])  # Ne x Nv
        self.vertex_features = tf.placeholder(tf.float32, [None, self.feat_v_train[0].shape[1]], name='fv')
        self.edge_features = tf.placeholder(tf.float32, [None, self.feat_e_train[0].shape[1]], name='fe')
        self.labels = tf.placeholder(tf.float32, [None], name='labels')  # ground-truth of probability
        self.label_mask = tf.placeholder(tf.int32, [None], name='label_mask')  # masking labels for MR subgraph
        self.is_train = tf.placeholder(tf.bool, name='is_train')
        self.normed_adj_v_matrix = tf.placeholder(tf.float32, [None, None], name='normed_v_matrix')
        self.normed_adj_ev_matrix = tf.placeholder(tf.float32, [None, None], name='normed_e_matrix')  # Ne x Nv
        self.partitions_e = tf.placeholder(tf.int32, [None], name='partitions_edge')
        self.partitions_v = tf.placeholder(tf.int32, [None], name='partitions_vertex')
        self.partitions_ev = tf.placeholder(tf.int32, [None], name='partitions_edge_and_vertex')

        self.preds = self.inference()

    def cal_loss(self):
        with tf.variable_scope('loss'):
            self.preds = tf.squeeze(self.preds, axis=1)
            preds = self.preds
            labels = self.labels
            # adj_incoming_e = self.incoming_e

            if self.conf.use_batch and (self.conf.reg_model == 'multi_logit'):
                label_mask = tf.cast(self.label_mask, tf.float32)
                preds = tf.multiply(preds, label_mask)
                labels = tf.multiply(labels, label_mask)

            self.mse = kdd_ops.loss_cost(preds, labels)
            self.regu_loss = 0
            for var in tf.trainable_variables():
                if all(word in var.name for word in ['weight', 'linear_output']):
                    self.regu_loss += self.conf.weight_decay * tf.nn.l2_loss(var) / var.shape.num_elements()
            self.loss_op = self.mse + self.regu_loss

    def config_summary(self, name):
        summarys = []
        summarys.append(tf.summary.scalar(name + '/loss', self.loss_op))
        summarys.append(tf.summary.scalar(name + '/mse', self.mse))
        if name == 'train':
            summarys.append(tf.summary.scalar(name + '/regu_loss', self.regu_loss))
        summary = tf.summary.merge(summarys)
        return summary

    def save_summary(self, summary, step):
        self.writer.add_summary(summary, step)

    def test(self):
        # self.reload(self.conf.load_step)
        fig_name = self.conf.modeldir + 'pred_true_test_latest.png'
        if self.conf.load_step is not None:
            checkpoint_path = os.path.join(
                self.conf.modeldir, self.conf.model_name)
            model_path = checkpoint_path + '-' + self.conf.load_step
            print('ckpt path:{}'.format(model_path))
            self.saver.restore(self.sess, model_path)
            fig_name = self.conf.modeldir + 'pred_true_test_' + str(self.conf.load_step) + '.png'
        else:
            if not os.path.exists(os.path.join(self.conf.modeldir, "latest.ckpt") + '.meta'):
                print('------- no such checkpoint', os.path.join(self.conf.modeldir, "latest.ckpt"))
                return
            print('ckpt path:{}'.format(self.conf.modeldir))
            self.saver.restore(self.sess, os.path.join(self.conf.modeldir, "latest.ckpt"))

        feed_test_dict = self.pack_dict('test', 0)
        pred, label = self.sess.run([self.preds, self.labels], feed_dict=feed_test_dict)
        pred = np.squeeze(pred)
        np.savetxt(self.conf.modeldir + "pred_true_test.csv", np.c_[pred, label], delimiter=",")
        self.cal_test_metrics(pred, label, 'test')
        x = np.linspace(0.0, 1.0, num=10)
        y = x
        plt.plot(x, y)
        plt.title('{}_wsize{}_stride{}_{}'.format(self.conf.dataset, self.conf.window_size,
                                                  self.conf.stride, self.conf.aggregator))
        plt.plot(label, pred, '.', label='test')
        plt.legend(loc=4)
        # plt.show()
        plt.savefig(fig_name)
        plt.clf()

    def train(self):
        import time
        if self.conf.reload_step > 0:
            self.reload(self.conf.reload_step)

        # k-fold cross-validation
        fold_loss = []
        if self.conf.k_fold:
            for fold in range(self.conf.spans - 1):
                fold_loss.append(self.kFold_train(fold))
            self.save_training_loss(fold_loss)
        else:
            self.transductive_train()

    def kFold_train(self, fold):
        # reinitialize variables for each fold
        self.sess.run(tf.global_variables_initializer())
        # feed validation dictionary based on fold value
        index = (fold + self.conf.spans - 2) % (self.conf.spans - 1)
        feed_val_dict = self.pack_dict('val', index)
        for epoch_num in range(self.conf.max_step + 1):
            for batch_index in range(self.conf.spans - 2):  # spans-1 graphs are used for training
                index = (fold + batch_index) % (self.conf.spans - 1)
                feed_train_dict = self.pack_dict('train', index)
                # feed_valid_dict = self.pack_dict_whole_graph('valid')
                train_loss, _ = self.sess.run(
                    [self.mse, self.train_op],
                    feed_dict=feed_train_dict)

            if epoch_num and epoch_num % 50 == 0:
                val_loss = self.sess.run(self.mse,
                                         feed_dict=feed_val_dict)
                print('fold: %d epoch: %d --- loss: %.4f, val: %.4f' % (
                    fold, epoch_num, train_loss, val_loss))
        val_loss = self.sess.run(self.mse,
                                 feed_dict=feed_val_dict)
        return val_loss

    def transductive_train(self):
        # stats = [0, 0, 0]
        total_time = 0
        feed_test_dict = self.pack_dict('test', 0)  # only one graph for testset
        min_train_loss, min_test_loss, train_loss, test_loss = 10, 10, 10, 10
        train_epoch, test_epoch = 0, 0
        for epoch_num in range(self.conf.max_step + 1):
            for batch_index in range(self.conf.spans - 1):  # spans-1 graphs are used for training
                feed_train_dict = self.pack_dict('train', batch_index)
                start_time = time.time()
                train_loss, _, summary = self.sess.run(
                    [self.mse, self.train_op, self.train_summary],
                    feed_dict=feed_train_dict)
                total_time += time.time() - start_time
                self.save_summary(summary, epoch_num + self.conf.reload_step)

            if epoch_num and epoch_num % 500 == 0:
                self.save(epoch_num)
            if epoch_num and epoch_num % self.conf.print_step == 0:
                if train_loss < min_train_loss:
                    min_train_loss, train_epoch = train_loss, epoch_num
                if epoch_num % self.conf.test_step == 0:
                    summary, pred, labels = self.sess.run(
                        [self.test_summary, self.preds, self.labels],
                        feed_dict=feed_test_dict)
                    # print('MR_preds shape: {}'.format(MR_preds.shape))
                    # pred = MR_preds * edge_class
                    # print('pred shape: {}'.format(pred.shape))
                    metrics = kdd_utils.cal_evl_metric(pred, labels)
                    test_loss = metrics[2]
                    self.save_summary(summary, epoch_num + self.conf.reload_step)
                    if test_loss < min_test_loss:
                        min_test_loss, test_epoch = test_loss, epoch_num
                        self.saver.save(self.sess, os.path.join(self.conf.modeldir, "latest.ckpt"))
                    print('epoch: %d --- train loss: %.5f, test loss: %.5f'
                          % (epoch_num, train_loss, test_loss))
                else:
                    print('epoch: %d --- train loss: %.5f' % (
                        epoch_num, train_loss))

        self.plot_pred_label(feed_train_dict, feed_test_dict, train_loss, test_loss, total_time)
        # self.save_losses(train_loss, test_loss, train_epoch, test_epoch, start_time)

    def cal_test_metrics(self, pred, label, phase, time=None, remove=False):
        pred = np.squeeze(pred)
        metrics = kdd_utils.cal_evl_metric(pred, label, remove)
        filename = './data/' + phase + '_' + self.conf.metric_name + '.csv'
        # save metrics to a csv file
        if not os.path.isfile(filename):
            writer = csv.writer(open(filename, 'w+', newline=''))
            res_summary = []
            res_summary.extend(
                ['dataset', 'prob name', 'window_size', 'stride', 'aggregator', 'gcl_linear', 'self_loop',
                 'reg_model', 'r2', 'mae', 'mse', 'mape', 'lr', 'ch_num', 'keep_r', 'l2', 'full_map',
                 'v_feat', 'k', 'fc_layer_num', 'max_degree', 'use_batch', 'batch_size', 'center_num',
                 'time'])
            writer.writerow(res_summary)
        else:
            writer = csv.writer(open(filename, 'a', newline=''))

        res_summary = []

        res_summary.extend([self.conf.dataset, self.conf.prob_name, self.conf.window_size, self.conf.stride,
                            self.conf.aggregator, self.conf.gcl_linear, self.conf.self_loop, self.conf.reg_model])
        for i in range(len(metrics)):
            print(metrics[i])
            res_summary.extend([metrics[i]])
        res_summary.extend([self.conf.learning_rate, self.conf.ch_num,
                            self.conf.keep_r, self.conf.weight_decay, self.conf.full_map,
                            self.conf.vertex_feat, self.conf.k, self.conf.fc_layer_num,
                            self.conf.max_degree, self.conf.use_batch, self.conf.batch_size,
                            self.conf.center_num, time])
        writer.writerow(res_summary)

    def plot_pred_label(self, train_dict, test_dict, train_loss, test_loss, time):
        import matplotlib.pyplot as plt
        # if self.conf.use_batch:
        train_values = self.sess.run([self.preds, self.labels], feed_dict=train_dict)
        self.cal_test_metrics(train_values[0], train_values[1], 'train', time)
        plt.plot(train_values[1], train_values[0], '.', label='train')
        x = np.linspace(0.0, 1.0, num=10)
        y = x
        plt.plot(x, y)
        plt.legend(loc=4)
        # plt.show()
        plt.savefig(self.conf.modeldir + 'pred_true_train.png')
        plt.clf()
        x = np.linspace(0.0, 1.0, num=10)
        y = x
        plt.plot(x, y)
        plt.text(0.01, 0.9, 'lr: %.4f, l2_weight: %.4f \ntrain mse: %.4f, test mse: %.4f' %
                 (self.conf.learning_rate, self.conf.weight_decay, train_loss, test_loss))
        plt.title('{}_wsize{}_stride{}_{}'.format(self.conf.dataset, self.conf.window_size,
                                                  self.conf.stride, self.conf.aggregator))
        preds, labels = self.sess.run([self.preds,
                                       self.labels], feed_dict=test_dict)

        np.savetxt(self.conf.modeldir + "pred_true.csv", np.c_[preds, labels], delimiter=",")
        self.cal_test_metrics(preds, labels, 'test', time)

    def save_losses(self, train_loss, test_loss, train_epoch, test_epoch, start_time):
        if not os.path.exists('./data/'):
            os.mkdir('./data/')
        writer = csv.writer(open('./data/training_loss_not_norm_feat.csv', 'a', newline=''))

        res_summary = []
        res_summary.extend([self.conf.dataset, self.conf.learning_rate, self.conf.ch_num,
                            self.conf.keep_r, self.conf.weight_decay, self.conf.gcl_linear, self.conf.full_map,
                            self.conf.vertex_feat, self.conf.window_size, self.conf.stride,
                            self.conf.aggregator, self.conf.k, self.conf.fc_layer_num,
                            self.conf.reg_model, self.conf.max_degree,
                            train_loss, train_epoch, test_loss, test_epoch, self.conf.use_batch, self.conf.batch_size,
                            self.conf.center_num, self.conf.self_loop, ((time.time() - start_time) / 60)])
        writer.writerow(res_summary)

    def save_training_loss(self, loss, filename='/k_fold_result_summary.csv'):
        import csv
        if not os.path.exists('./data/' + self.conf.dataset):
            os.mkdir('./data/' + self.conf.dataset)
        writer = csv.writer(open('./data/' + self.conf.dataset + filename, 'a', newline=''))
        # f = open(, 'a')
        res_summary = []
        res_summary.extend([self.conf.dataset, self.conf.window_size, self.conf.stride,
                            self.conf.keep_r, self.conf.weight_decay,
                            self.conf.k, self.conf.ch_num, self.conf.fc_layer_num,
                            self.conf.gcl_linear, self.conf.full_map,
                            self.conf.vertex_feat, self.conf.learning_rate, self.conf.aggregator,
                            max(loss), min(loss), sum(loss) / len(loss)])
        writer.writerow(res_summary)

    def pack_dict(self, action, index):
        feed_dict = {self.adj_edge: self.edge_connect,
                     self.incoming_e: self.adj_incoming_e,
                     self.outgoing_e: self.adj_outgoing_e, self.normed_adj_v_matrix: self.normed_adj_v,
                     self.normed_adj_ev_matrix: self.normed_adj_ev, self.partitions_v: self.parts_v_mask,
                     self.partitions_e: self.parts_e_mask, self.partitions_ev: self.parts_ev_mask}
        if action == 'train':
            feed_dict.update({
                self.vertex_features: self.feat_v_train[index],
                self.edge_features: self.feat_e_train[index], self.labels: self.labels_train[index],
                self.is_train: True})
            if self.conf.use_batch:
                if self.conf.fixed_sub:
                    v_indice, e_indice, mask = get_sampled_index(self.normed_adj_v,
                                                                 self.normed_adj_ev,
                                                                 self.conf.batch_size, self.conf.center_num)
                else:
                    v_indice, e_indice = get_indice_connected_subgraph(self.normed_adj_v,
                                                                       self.normed_adj_ev,
                                                                       self.conf.batch_size, self.conf.center_num)
                    mask = np.ones_like(e_indice)
                '''if subgraph is used, no need to slicing on adj_matrices for training, 
                and n_slice will be set to 1 in aggregator'''
                parts_v = np.zeros(len(v_indice))
                parts_e = np.zeros(len(e_indice))
                parts_ev = np.hstack([parts_e, parts_v])

                feed_dict.update({
                    self.adj_edge: self.edge_connect[e_indice, :][:, e_indice],
                    self.incoming_e: self.adj_incoming_e[e_indice, :][:, v_indice],
                    self.outgoing_e: self.adj_outgoing_e[e_indice, :][:, v_indice],
                    self.vertex_features: self.feat_v_train[index][v_indice],
                    self.edge_features: self.feat_e_train[index][e_indice],
                    self.labels: self.labels_train[index][e_indice],
                    self.normed_adj_v_matrix: self.normed_adj_v[v_indice, :][:, v_indice],
                    self.normed_adj_ev_matrix: self.normed_adj_ev[e_indice, :][:, v_indice],
                    self.partitions_v: parts_v,
                    self.partitions_e: parts_e,
                    self.partitions_ev: parts_ev, self.label_mask: mask})
        elif action == 'val':
            feed_dict.update({
                # self.adj_vertex: self.normed_adj_v[index], self.adj_edge_vertex: self.normed_adj_ev[index],
                self.vertex_features: self.feat_v_train[index],
                self.edge_features: self.feat_e_train[index], self.labels: self.labels_train[index],
                self.is_train: False})
            if self.conf.use_batch:
                mask = np.ones_like(self.labels_train[index])
                feed_dict.update({self.label_mask: mask})
            # if self.conf.use_batch:
            #     raise NotImplementedError('subgraph To be implemented!')
        elif action == 'test':
            feed_dict.update({
                # self.adj_vertex: self.normed_adj_v_test[index], self.adj_edge_vertex: self.normed_adj_e_test[index],
                self.vertex_features: self.feat_v_test[index],
                self.edge_features: self.feat_e_test[index], self.labels: self.labels_test[index],
                self.is_train: False})
            if self.conf.use_batch:
                mask = np.ones_like(self.labels_test[index])
                feed_dict.update({self.label_mask: mask})
        return feed_dict

    def save(self, step):
        print('---->saving', step)
        checkpoint_path = os.path.join(
            self.conf.modeldir, self.conf.model_name)
        self.saver.save(self.sess, checkpoint_path, global_step=step)

    def reload(self, step):
        checkpoint_path = os.path.join(
            self.conf.modeldir, self.conf.model_name)
        model_path = checkpoint_path + '-' + str(step)
        if not os.path.exists(model_path + '.meta'):
            print('------- no such checkpoint', model_path)
            return
        self.saver.restore(self.sess, model_path)

    def print_params_num(self):
        total_params = 0
        for var in tf.trainable_variables():
            print(var)
            total_params += var.shape.num_elements()
        print("The total number of params --------->", total_params)
