import pandas as pd
import os
import numpy as np


def combine_dfs(twt_df, retwt_df, threshold=5):
    # drop multiple retweet on one tweet
    if retwt_df.duplicated(['user_id', 'original_twtid']).any():
        print('duplicated retweeting on one tweet!!')
        retwt_df = retwt_df.drop_duplicates(['user_id', 'original_twtid'], keep='first')
    # get the total retwts for the d_user over a certain time period
    tweet_df = twt_df.groupby(['user_id']).apply(groupbyTwt).reset_index()
    # drop users with very few tweets/retweets (less than threshold)
    tweet_df = tweet_df.loc[tweet_df['total_twts'] >= threshold]
    merged_df = retwt_df.merge(tweet_df, left_on=['user_id'],
                               right_on=['user_id'], how='inner')
    # get the statistic on source user (tweet)
    merged_df = merged_df.merge(tweet_df, left_on=['retweet_from'],
                                right_on=['user_id'], how='inner', suffixes=('_d', '_s'))
    print('merged_df columns: {}'.format(list(merged_df)))

    retwts = merged_df.groupby(['user_id_d']).size().reset_index(name='total_rewts_d')
    retweet_df = merged_df.groupby(['user_id_d', 'user_id_s']).apply(groupbyreTwt).reset_index()
    print('retweet_df shape before merge :{}'.format(retweet_df.shape))
    merged_df = retweet_df.merge(retwts, left_on=['user_id_d'],
                                 right_on=['user_id_d'], how='inner')
    print('retweet_df shape before merge :{}'.format(merged_df.shape))

    merged_df['bern_prob'] = merged_df['retwts'] / merged_df['total_twts_s']
    merged_df['jaccard_prob'] = \
        merged_df['retwts'] / (merged_df['total_twts_s'] + merged_df['total_twts_d'] - merged_df['retwts'])
    merged_df['lt_prob'] = merged_df['retwts'] / merged_df['total_rewts_d']

    merged_df.loc[merged_df['bern_prob'] > 1.0, ['bern_prob']] = 1.0
    merged_df.loc[merged_df['jaccard_prob'] > 1.0, ['jaccard_prob']] = 1.0
    merged_df.loc[merged_df['lt_prob'] > 1.0, ['lt_prob']] = 1.0

    print('bern_prob max:{}'.format(merged_df['bern_prob'].max()))
    if merged_df['bern_prob'].max() > 1.0:
        index = merged_df['bern_prob'].idxmax()
        s = merged_df.iloc[[index]]['user_id_s']
        d = merged_df.iloc[[index]]['user_id_d']
        print('source user: {}, target user: {}'.format(s, d))
    print('jaccard_prob max:{}'.format(merged_df['jaccard_prob'].max()))
    print('lt_prob max:{}'.format(merged_df['lt_prob'].max()))
    merged_df.to_csv('new_df.csv', index=False)
    return merged_df


def gen_retwt_record(maindir, dataset, stride, size, periods):
    # saved raw tweets information
    maindir += dataset + '/'
    temp_filename = maindir + dataset + '_raw_twts.pkl'
    df = pd.read_pickle(temp_filename)

    retweet_df = df.loc[df['retweet_flag'] == True]
    print('retweet_df shape before remove self_loop: {}'.format(retweet_df.shape))
    retweet_df = retweet_df.loc[retweet_df['user_id'] != retweet_df['retweet_from']]
    print('retweet_df shape after remove self_loop: {}'.format(retweet_df.shape))
    retweet_df = cal_Adamic_Adar(retweet_df)
    tweet_df = df[['tweet_id', 'user_id', 'd_friendsCount', 'd_followersCount', 'year', 'month']]

    old_tweet_df = retweet_df[['original_twtid', 'retweet_from', 's_friendsCount', 's_followersCount', 'original_time']]
    old_tweet_df = old_tweet_df.assign(
        time=lambda x: pd.to_datetime(x['original_time']))  # convert time to year, month, day
    old_tweet_df = old_tweet_df.assign(year=old_tweet_df.time.dt.year)
    old_tweet_df = old_tweet_df.assign(month=old_tweet_df.time.dt.month)
    old_tweet_df.drop(['time', 'original_time'], axis=1, inplace=True)

    old_tweet_df = old_tweet_df.rename(index=str, columns={'original_twtid': 'tweet_id',
                                                           'retweet_from': 'user_id',
                                                           's_friendsCount': 'd_friendsCount',
                                                           's_followersCount': 'd_followersCount'})
    all_tweet_df = pd.concat([tweet_df, old_tweet_df], sort=False)
    all_tweet_df = all_tweet_df.drop_duplicates(['tweet_id'], keep='first')
    if all_tweet_df.duplicated('tweet_id').any():
        print('duplicated tweet_id still identified!!')

    retweet_df = retweet_df.assign(time=lambda x: x['year'].astype(str) + '-' + x['month'].astype(str))

    all_tweet_df = all_tweet_df.assign(time=lambda x: x['year'].astype(str) + '-' + x['month'].astype(str))
    # print('time in all_tweet_df: {}'.format(sorted(retweet_df['time'].unique())))
    # print('unique time in all_tweet_df: {}'.format(sorted(all_tweet_df['time'].unique())))
    #
    all_tweet_df.loc[~all_tweet_df['time'].isin(periods), 'time'] = periods[0]

    mydir = maindir + "size_" + str(size) + "_stride_" + str(stride) + "/"

    if not os.path.exists(mydir):
        os.mkdir(mydir)
    for period in range(0, int((len(periods) - size) / stride) + 1):
        start_index = period * stride
        end_index = start_index + size
        temp_twt_df = all_tweet_df.loc[all_tweet_df['time'].isin(periods[start_index:end_index])]
        temp_retwt_df = retweet_df.loc[retweet_df['time'].isin(periods[start_index:end_index])]
        print("unique months in temp_twt_df: {}".format(temp_twt_df['time'].unique()))
        print("unique months in temp_retwt_df: {}".format(temp_retwt_df['time'].unique()))

        temp_df = combine_dfs(temp_twt_df, temp_retwt_df, 5)
        print("columns in temp_retwt_df: {}".format(list(temp_df)))
        retwt_filename = mydir + dataset + "_retwt_period_" + str(period) + '_stride_' + \
                         str(stride) + '_size_' + str(size) + ".pkl"
        temp_df.to_pickle(retwt_filename)


def cal_Adamic_Adar(retwt_df):
    retweeted_times_df = retwt_df.groupby(['original_twtid']).size().reset_index(name='counts')
    retweeted_times_df['score'] = 1 / np.log(retweeted_times_df['counts'] + 1)
    retwt_df = retwt_df.merge(retweeted_times_df, on=['original_twtid'], how='left')
    return retwt_df


def groupbyTwt(twt_df):
    temp_df = {}
    temp_df['total_twts'] = twt_df['user_id'].count()
    # no matter it is a retweet or an original twt, the friendscount
    # and followers count are saved in d_...
    temp_df['friends_avg'] = twt_df['d_friendsCount'].mean()
    temp_df['follower_avg'] = twt_df['d_followersCount'].mean()
    return pd.Series(temp_df)


def groupbyreTwt(retwt_df):
    temp_df = {}
    temp_df['friends_avg_s'] = retwt_df['s_friendsCount'].mean()
    temp_df['friends_avg_d'] = retwt_df['d_friendsCount'].mean()
    temp_df['follower_avg_s'] = retwt_df['s_followersCount'].mean()
    temp_df['follower_avg_d'] = retwt_df['d_followersCount'].mean()
    temp_df['total_twts_s'] = retwt_df['total_twts_s'].mean()
    temp_df['total_twts_d'] = retwt_df['total_twts_d'].mean()
    temp_df['retwts'] = retwt_df['user_id_d'].count()
    temp_df['adamic_adar'] = retwt_df['score'].sum()

    # temp_df['d_follower_avg'] = retwt_df['d_followersCount'].mean()
    return pd.Series(temp_df)


if __name__ == "__main__":
    temp_twt_df = pd.read_csv('twt_df.csv')
    temp_retwt_df = pd.read_csv('retwt_df.csv')
    temp_df = combine_dfs(temp_twt_df, temp_retwt_df, 5)
    maindir = "../data/"
    dataset = "CVE"
    size, stride = 4, 2
    periods = ['2017-6', '2017-7', '2017-8', '2017-9', '2017-10', '2017-11',
               '2017-12', '2018-1', '2018-2', '2018-3', '2018-4', '2018-5']

    gen_retwt_record(maindir, dataset, stride, size, periods)
