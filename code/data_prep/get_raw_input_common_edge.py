import pandas as pd
import argparse
import os
import gen_retwt_record, gen_adj_feat_m
import numpy as np


def data_prep(args):
    # if not os.path.exists(args.dir):
    #     raise IOError('path {} not exist!'.format(args.dir))
    periods = []
    if args.dataset == 'CVE':
        periods = ['2017-6', '2017-7', '2017-8', '2017-9', '2017-10', '2017-11',
                   '2017-12', '2018-1', '2018-2', '2018-3', '2018-4', '2018-5']
    elif args.dataset == 'cybersecurity':
        periods = ['2016-7', '2016-8', '2016-9', '2016-10', '2016-11',
                   '2016-12', '2017-1', '2017-2', '2017-3', '2017-4', '2017-5']
    elif args.dataset.lower() == 'ransomware':
        periods = ['2016-7', '2016-8', '2016-9', '2016-10', '2016-11',
                   '2016-12', '2017-1', '2017-2', '2017-3', '2017-4', '2017-5']
    else:
        raise NotImplementedError('not supported to the dataset {}'.format(args.dataset))
    #if not os.path.exists(args.dir + args.dataset + "/size_" + str(args.window_size) + "_stride_" + str(args.stride) + "/"):
    if args.preprocess:
        # if specified dataroot not exist, first preprocess data
        gen_retwt_record.gen_retwt_record(args.dir, args.dataset, args.stride, args.window_size, periods)

    total_period = int((len(periods) - args.window_size) / args.stride) + 1
    period = 0

    args.dir += args.dataset + "/size_" + str(args.window_size) + "_stride_" + str(args.stride) + "/"
    args.output_dir += args.dataset + "/size_" + str(args.window_size) + "_stride_" + str(args.stride) + "/"
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    retwt_filename = args.dir + args.dataset + "_retwt_period_" + str(period) + '_stride_' + \
                     str(args.stride) + '_size_' + str(args.window_size) + ".pkl"
    retwt = pd.read_pickle(retwt_filename)
    print('edge in one df: {}'.format(retwt.shape[0]))

    # edges = pd.DataFrame()
    if args.common_edge:
        edges, usermap = find_common_edge(retwt, total_period, args)
        if args.draw_fig_4raw_data:
            draw_fig(edges, total_period, args, 'follower_avg_d', 0)
            #draw_fig(edges, total_period, args, 'follower_avg_s', 0)
        else:
            gen_raw_data_4GCN(edges, usermap, total_period, args)
    else:
        print('to be implemented for not common edges!')


def find_common_edge(retwt_df, total_period, args):
    common_edge = retwt_df[['user_id_s', 'user_id_d']]
    for period in range(1, total_period):
        retwt_filename = args.dir + args.dataset + "_retwt_period_" + str(period) + '_stride_' + \
                         str(args.stride) + '_size_' + str(args.window_size) + ".pkl"
        temp_retwt = pd.read_pickle(retwt_filename)
        edge = temp_retwt[['user_id_s', 'user_id_d']]
        # inner merge to find common edge
        common_edge = common_edge.merge(edge, on=['user_id_s', 'user_id_d'],
                                        how='inner')
    userID = pd.unique(common_edge[['user_id_s', 'user_id_d']].values.ravel('K'))
    usermap = dict([(y, x) for x, y in enumerate(sorted(set(userID)))])
    common_edge.to_csv(args.output_dir + args.dataset + "common_edge.csv", index=False)
    print('# of common edge {} for {} with window size {} stride {}'
          .format(common_edge.shape, args.dataset, args.window_size, args.stride))
    return common_edge, usermap


def draw_fig(selected_edges, total_period, args, column, num):
    import matplotlib.pyplot as plt
    feat_df = pd.DataFrame()
    for period in range(0, total_period):
        edges = selected_edges
        retwt_filename = args.dir + args.dataset + "_retwt_period_" + str(period) + \
                         '_stride_' + str(args.stride) + '_size_' + str(args.window_size) + ".pkl"
        print(retwt_filename)
        temp_retwt = pd.read_pickle(retwt_filename)
        temp_prob = temp_retwt[['user_id_s', 'user_id_d', column]]
        edges = edges.merge(temp_prob, on=['user_id_s', 'user_id_d'],
                            how='inner', suffixes=(str(period - 1), str(period)))

        print("prob df shape: {}".format(edges.shape))

        if feat_df.empty:
            feat_df = edges
        else:
            print("feat_df shape: {}".format(feat_df.shape))
            feat_df = feat_df.merge(edges, on=['user_id_s', 'user_id_d'],
                            how='inner', suffixes=(str(period - 1), str(period)))
        # print("train sample size: {}".format(feat_df.shape))
    print("column names of feat_df {}".format(list(feat_df)))
    # feat_df.to_csv(args.dir + args.dataset + "_rawdata_" + column +
    #               ".csv", index=False)
    feat_df.drop(feat_df.iloc[:, 0:2], inplace=True, axis=1)
    length = 20
    if feat_df.shape[0] < length:
        length = feat_df.shape[0]
    feat_df.T.iloc[:, num:num+length].plot()
    plt.title(column)
    plt.savefig(args.dir + args.dataset + "_" + column + ".png")


def gen_raw_data_4GCN(selected_edges, usermap, total_period, args):
    for prob_name in ('bern_prob', 'jaccard_prob', 'lt_prob'):
        for period in range(0, total_period - args.num_prob):
            feat_df = pd.DataFrame()
            edges = selected_edges
            temp_retwt = pd.DataFrame()
            for step in range(args.num_prob + 1):
                retwt_filename = args.dir + args.dataset + "_retwt_period_" + str(period + step) + \
                                 '_stride_' + str(args.stride) + '_size_' + str(args.window_size) + ".pkl"
                print(retwt_filename)
                temp_retwt = pd.read_pickle(retwt_filename)
                # print(list(temp_retwt))
                temp_prob = temp_retwt[['user_id_s', 'user_id_d', prob_name]]
                edges = edges.merge(temp_prob, on=['user_id_s', 'user_id_d'],
                                    how='inner')

            print("prob df shape: {}".format(edges.shape))
            temp_feat_df = temp_retwt[['user_id_s', 'user_id_d', 'friends_avg_s', 'friends_avg_d', 'follower_avg_s',
                                       'follower_avg_d', 'total_twts_s', 'total_twts_d', 'retwts', 'adamic_adar']]
            # temp_feat_df['ada'] = np.log()
            temp_feat_df = temp_feat_df.merge(edges, on=['user_id_s', 'user_id_d'],
                                              how='inner')
            print("temp_feat_df shape: {}".format(temp_feat_df.shape))
            if feat_df.empty:
                feat_df = temp_feat_df
            else:
                print("feat_df shape: {}".format(feat_df.shape))
                feat_df = pd.concat([feat_df, temp_feat_df], sort=False)
            # print("train sample size: {}".format(feat_df.shape))
            print("column names of feat_df {}".format(list(feat_df)))
            if prob_name == 'lt_prob':
                # renormalize LT prob after removing non-common edges
                feat_df['lt_sum_x'] = feat_df.groupby('user_id_d')['lt_prob_x'].transform(np.sum)
                feat_df['lt_norm_x'] = feat_df['lt_prob_x'] / feat_df['lt_sum_x']
                feat_df['lt_sum_y'] = feat_df.groupby('user_id_d')['lt_prob_y'].transform(np.sum)
                feat_df['lt_norm_y'] = feat_df['lt_prob_y'] / feat_df['lt_sum_y']
                feat_df.drop(['lt_prob_x', 'lt_sum_x', 'lt_prob_y', 'lt_sum_y'], axis=1, inplace=True)
                feat_df = feat_df.rename(index=str, columns={'lt_norm_x': 'lt_prob_x',
                                                             'lt_norm_y': 'lt_prob_y'})
            sentiment_filename = args.dir + args.dataset + "_twt_content_period_" + str(period) + \
                                 '_stride_' + str(args.stride) + '_size_' + str(args.window_size) + ".csv"
            sentiment_score = load_sentiment_score(sentiment_filename)
            sentiment_score['user_id'] = sentiment_score['user_id'].astype(str)

            feat_df = feat_df.merge(sentiment_score, left_on=['user_id_s'], right_on=['user_id'], how='left')
            feat_df = feat_df.merge(sentiment_score, left_on=['user_id_d'], right_on=['user_id'], how='left',
                                    suffixes=('_1', '_2'))
            feat_df = feat_df.fillna(0.0)
            feat_df = feat_df[
                ['user_id_s', 'user_id_d', 'friends_avg_s', 'friends_avg_d', 'follower_avg_s', 'follower_avg_d',
                 'total_twts_s', 'total_twts_d', 'sentiment_1', 'sentiment_2', 'retwts', 'adamic_adar',
                 prob_name + '_x', prob_name + '_y']]
            feat_df.insert(loc=0, column='s_user', value=feat_df['user_id_s'].map(usermap))
            feat_df.insert(loc=1, column='d_user', value=feat_df['user_id_d'].map(usermap))
            # feat_df = feat_df.drop(['user_id_s', 'user_id_d'], axis=1)
            feat_df = feat_df.dropna()
            feat_df = feat_df.sort_values(by=['s_user', 'd_user'])
            feat_df.to_csv(args.output_dir + args.dataset + "_input_" + str(prob_name) + "_span_" + str(period) +
                           ".csv", index=False)

            data = gen_adj_feat_m.gen_adjs(feat_df, usermap)
            filename = args.output_dir + args.dataset + "_input_" + str(prob_name) + "_span_" + str(period) + ".npz"
            f_handle = open(filename, 'wb')
            print('vertex no. {}, edge no. {}, edge no. {}'.format(data[0].shape[0], data[1].shape[0], data[2].shape[0]))
            np.savez(f_handle,
                     Av=data[0],
                     Ae=data[1],
                     edge_connectivity=data[2],
                     fv=data[3],
                     fe=data[4],
                     label=data[5])
            f_handle.close()


def load_sentiment_score(filename):
    df = pd.read_csv(filename)
    df = df.groupby('user_id')['sentiment'].mean().reset_index()
    df['sentiment'] = df['sentiment'] # + 4.0
    return df


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='data preparing.')
    parser.add_argument('--dir', '-dir', type=str, default='./data/', help='path to data file')
    parser.add_argument('--dataset', '-d', type=str, default='CVE', help='dataset to be processed')
    parser.add_argument('--common_edge', '-ce', action="store_true", help='use common edge')
    parser.add_argument('--preprocess', '-pre', action="store_true", help='need preprocess or not')
    parser.add_argument('--stride', '-s', type=int, default=2, help='stride between two time windows')
    parser.add_argument('--window_size', '-w', type=int, default=6, help='sliding window size')
    parser.add_argument('--num_prob', '-np', type=int, default=1,
                        help='number of probabilities in previous spans used for predict')
    parser.add_argument('--draw_fig_4raw_data','-draw', default=False)
    parser.add_argument('--output_dir', '-o', type=str, default='./data/sentiment_incld/')

    args = parser.parse_args()
    data_prep(args)
