import numpy as np
import pandas as pd
import scipy.sparse as sparse
'''Save two edge connectivity file (Ne x Ne), one include all incoming edges, 
the other one exclude the incoming edge that have share the same vertexes'''


def gen_adjs(act_log, usermap):
    act_log, twt_records = add_id_to_df(act_log, usermap)
    n_users = twt_records.shape[0]
    print('twt_records columns: {}'.format(list(twt_records)))
    twt_records = twt_records.sort_values('user_u_id')
    feat_vertex = twt_records.values
    # feat_vertex: no. of friends, no. of followers, total_twts
    feat_vertex = feat_vertex[:,1:]  # remove the first column, which is the vertex id, Nv x dv
    nrows = act_log.shape[0]

    A_vertex = sparse.coo_matrix((np.ones(nrows), (act_log['s_user'], act_log['d_user'])),
                                 shape=(n_users, n_users))  # Nv x Nv
    A_vertex = A_vertex.toarray()

    data = np.hstack((-np.ones(nrows), np.ones(nrows)))
    row = np.hstack((np.arange(nrows), np.arange(nrows)))
    col = np.hstack((act_log['s_user'].values, act_log['d_user'].values))
    A_edge = sparse.coo_matrix((data, (row, col)),
                               shape=(nrows, n_users))  # Ne x Nv
    A_edge = A_edge.toarray()
    edge_connect = gen_edge_connectivity(A_edge)  # Ne x Ne
    df_edge_feat = act_log.iloc[:, -4:-1]
    # edge feat: no. of retwts, adamic_adar, previous p
    feat_edge = df_edge_feat.values  # Ne x de
    feat_edge = feat_edge.reshape((A_edge.shape[0], -1))

    label = act_log.iloc[:, -1].values  # Ne x 1
    # label = label.reshape((A_edge.shape[0], -1))
    return A_vertex, A_edge, edge_connect, feat_vertex, feat_edge, label


def add_id_to_df(act_log, usermap):
    userID = pd.unique(act_log[['s_user', 'd_user']].values.ravel('K'))
    # # usermap = dict([(y, x) for x, y in enumerate(sorted(set(userID)))])
    usermap1 = dict([(y, x) for x, y in enumerate(userID)])
    assert len(usermap1) == len(usermap)
    # act_log.insert(loc=0, column='s_user', value=act_log['user_id_s'].map(usermap))
    # act_log.insert(loc=1, column='d_user', value=act_log['user_id_d'].map(usermap))
    # act_log = act_log.drop(['user_id_s', 'user_id_d'], axis=1)
    # act_log = act_log.dropna()
    twt_records = derive_user_info(act_log)
    print('twt_records.shape[0]: {}, len of usermap: {}'.format(twt_records.shape[0], len(usermap)))
    assert twt_records.shape[0] == len(usermap)  # all users in the usermap should be in twt_records
    return act_log, twt_records


def derive_user_info(act_log):
    user_s = act_log[['s_user', 'friends_avg_s', 'follower_avg_s', 'total_twts_s', 'sentiment_1']]
    user_s = user_s.rename(index=str, columns={'s_user': 'user_u_id',
                                               'friends_avg_s': 'friends',
                                               'follower_avg_s': 'followers',
                                               'total_twts_s': 'total_twts',
                                               'sentiment_1': 'sentiment'})
    user_d = act_log[['d_user', 'friends_avg_d', 'follower_avg_d', 'total_twts_d', 'sentiment_2']]
    user_d = user_d.rename(index=str, columns={'d_user': 'user_u_id',
                                               'friends_avg_d': 'friends',
                                               'follower_avg_d': 'followers',
                                               'total_twts_d': 'total_twts',
                                               'sentiment_2': 'sentiment'})

    user = pd.concat([user_s, user_d])
    user = user.drop_duplicates(['user_u_id'])
    return user


def gen_edge_connectivity(A_edge):  # generate adj_edge matrix for k-pooling edge features
    outgoing_edge = np.copy(A_edge)
    outgoing_edge[outgoing_edge == 1] = 0
    index = np.argmax(abs(outgoing_edge), axis=1)

    incoming_edge = np.copy(A_edge)
    incoming_edge[incoming_edge == -1] = 0

    edge_connectivity = incoming_edge[:, index].T
    return edge_connectivity


if __name__ == "__main__":
    my_act_log = pd.read_csv("./data/CVE/size_4_stride_2/CVE_input_bern_prob_span_0.csv")
    # my_twt_log = pd.read_csv("./data/debug_data/twt_log.csv")
    data = gen_adjs(my_act_log)

    '''A_vertex, A_edge, edge_connect, feat_vertex, feat_edge, label'''
    print('Av:{}'.format(data[0]))
    print('Ae:{}'.format(data[1]))
    print('adj_edge:{}'.format(data[2]))
    print('fv:{}'.format(data[3]))
    print('fe:{}'.format(data[4]))
    print('label:{}'.format(data[5]))


