import tensorflow as tf


def graph_embedding(adj_ev, feat_v, feat_e,
                    num_out, keep_r, is_train, use_batch, scope, n_slice, partitions_e, partitions_v, # embedding
                    act_fn=tf.nn.elu, aggregator='max_pooling', **kw):
    embed_v = aggregator_vertex(adj_ev, feat_e, feat_v, 1, scope + '/v_' + aggregator,
                                aggregator, n_slice, partitions_e, is_train, use_batch, mode='embedding')
    embed_e = aggregator_edge(adj_ev, feat_v, feat_e, 1, scope + '/e_' + aggregator,
                              aggregator, n_slice, partitions_v, is_train, use_batch, mode='embedding')

    embed_v = tf.squeeze(embed_v, axis=[1])
    hidden_v = linear(embed_v, num_out,  keep_r, is_train, scope+'/vertex', act_fn)

    embed_e = tf.squeeze(embed_e, axis=[1])
    hidden_e = linear(embed_e, num_out, keep_r, is_train, scope + '/edge', act_fn)

    return hidden_v, hidden_e


def graph_conv(adj_v, adj_incoming, adj_outgoing, feat_v, feat_e, edge_connect,
               num_out, adj_keep_r, keep_r, is_train, use_batch, scope, n_slice, partitions, k=1,
               act_fn=tf.nn.relu6, aggregator='max_pooling', **kw):
    num_in = feat_v.shape[-1].value  # feat_v: Nv x d

    adj_v = dropout(adj_v, adj_keep_r, is_train, scope+'/drop_av')
    adj_incoming = dropout(adj_incoming, adj_keep_r, is_train, scope + '/drop_incoming_e')
    adj_outgoing = dropout(adj_outgoing, adj_keep_r, is_train, scope + '/drop_outgoing_e')
    edge_connect = dropout(edge_connect, adj_keep_r, is_train, scope + '/drop_outgoing_e')

    adj_v_k = tf.concat([adj_v, adj_incoming], axis=0, name=scope + '/concat_v_k')
    feat_all = tf.concat([feat_v, feat_e], axis=0, name=scope + '/concat_v_k')
    adj_e_k = tf.concat([adj_outgoing, edge_connect], axis=1, name=scope + '/concat_v_k')

    if aggregator == 'max_pooling':
        hidden_v = aggregator_vertex(adj_v_k, feat_all, feat_v, k, scope + '/v_' + aggregator,
                                    aggregator, n_slice, partitions, is_train, use_batch, mode='conv')
        hidden_e = aggregator_edge(adj_e_k, feat_all, feat_e, k, scope + '/e_' + aggregator,
                                  aggregator, n_slice, partitions, is_train, use_batch, mode='conv')
    elif aggregator in ('mean', 'LSTM'):
        k = 1
        hidden_v = aggregator_vertex(adj_v_k, feat_all, feat_v, k, scope + '/v_' + aggregator,
                                     aggregator, n_slice, partitions, is_train, use_batch, mode='conv')
        hidden_e = aggregator_edge(adj_e_k, feat_all, feat_e, k, scope + '/e_' + aggregator,
                                   aggregator, n_slice, partitions, is_train, use_batch, mode='conv')
    else:
        raise NotImplementedError('aggregator {} is not supported!'.format(aggregator))

    outs_v = dropout(hidden_v, keep_r, is_train, scope + '/drop1_v')
    outs_e = dropout(hidden_e, keep_r, is_train, scope + '/drop1_e')

    outs_v = conv1d(outs_v, (num_in + num_out) // 2, (k + 1) // 2 + 1, scope + '/conv1_v', None, True)
    outs_v = act_fn(outs_v, scope + 'act1_v') if act_fn else outs_v
    outs_v = dropout(outs_v, keep_r, is_train, scope + '/drop2_v')
    outs_v = conv1d(outs_v, num_out, k // 2 + 1, scope + '/conv2_v', None)
    outs_v = tf.squeeze(outs_v, axis=[1], name=scope + '/squeeze_v')
    outs_v = batch_norm(outs_v, is_train, scope + '/norm2_v', act_fn)

    outs_e = conv1d(outs_e, (num_in + num_out) // 2, (k + 1) // 2 + 1, scope + '/conv_e', None, True)
    outs_e = act_fn(outs_e, scope + 'act1_e') if act_fn else outs_e
    outs_e = dropout(outs_e, keep_r, is_train, scope + '/drop2_e')
    outs_e = conv1d(outs_e, num_out, k // 2 + 1, scope + '/conv2_e', None)
    outs_e = tf.squeeze(outs_e, axis=[1], name=scope + '/squeeze_e')
    outs_e = batch_norm(outs_e, is_train, scope + '/norm2_e', act_fn)

    return outs_v, outs_e


def linear(feat, num_out, keep_r, is_train, scope, act_fn=tf.nn.elu):
    # adj = dropout(adj, adj_keep_r, is_train, scope + '/drop1')  # Nv x Nv
    outs = dropout(feat, keep_r, is_train, scope + '/drop2')  # Nv x (dv+de)
    hidden = fully_connected(outs, num_out, scope + '/fully', None)  # Nv x num_out

    hidden = hidden if not act_fn else act_fn(hidden, scope + '/act')
    # hidden = batch_norm(hidden, is_train, scope + '/norm', act_fn=None)
    return hidden


def fully_connected(outs, dim, scope, act_fn=tf.nn.elu):
    outs = tf.contrib.layers.fully_connected(
        outs, dim, activation_fn=None, scope=scope+'/dense',
        weights_initializer=tf.contrib.layers.xavier_initializer(),
        biases_initializer=tf.contrib.layers.xavier_initializer())
        # weights_initializer=tf.random_normal_initializer(),
        #biases_initializer=tf.random_normal_initializer())
    return act_fn(outs, scope+'/act') if act_fn else outs


def batch_norm(outs, is_train, scope, act_fn=tf.nn.relu6):
    return tf.contrib.layers.batch_norm(
        outs, scale=True,
        activation_fn=act_fn, fused=True,
        is_training=is_train, scope=scope,
        updates_collections=None)


def dropout(outs, keep_r, is_train, scope):
    if keep_r < 1.0:
        return tf.contrib.layers.dropout(
            outs, keep_r, is_training=is_train, scope=scope)
    return outs


def conv1d(outs, num_out, k, scope, act_fn=tf.nn.relu6, use_bias=False):
    l2_func = tf.contrib.layers.l2_regularizer(5e-4, scope)
    outs = tf.layers.conv1d(
        outs, num_out, k, activation=act_fn, name=scope+'/conv',
        padding='valid', use_bias=use_bias,
        kernel_initializer=tf.contrib.layers.xavier_initializer())
    return outs


def aggregator_vertex(adj_m, fea_m, fea_v, k, scope, aggregator, n_slice, partitions, is_train, use_batch, mode='conv'):
    n = tf.shape(adj_m)[0]

    def subgraph_agg():
        # n_slice = 1
        return select_vertex_neighbors_slice(adj_m, fea_m, k, scope, aggregator)

    def slice_agg():
        return aggregate()

    def aggregate():
        adj_m_parts = tf.dynamic_partition(adj_m, partitions, n_slice)
        feat_m_parts = tf.dynamic_partition(fea_m, partitions, n_slice)
        # print('n_slice: {}'.format(n_slice))

        # gather_idx = tf.range(slice_size)
        global_top_k_result = select_vertex_neighbors_slice(adj_m_parts[0], feat_m_parts[0],
                                                            k, scope, aggregator)

        for i in range(1, n_slice):
            sub_adj_m = adj_m_parts[i]
            sub_feat_m = feat_m_parts[i]
            local_top_k_result = select_vertex_neighbors_slice(sub_adj_m, sub_feat_m, k, scope, aggregator)

            if aggregator == 'max_pooling':
                global_top_k_result = tf.nn.top_k(tf.concat([global_top_k_result, local_top_k_result], axis=-1),
                                                  k=k, name=scope + '/ngb_top_k').values
            elif aggregator in ('mean', 'LSTM'):
                global_top_k_result += local_top_k_result
        return global_top_k_result

    if use_batch:
        # if subgraph is used for training, slicing will not needed.
        global_top_k_result = tf.cond(is_train, subgraph_agg, slice_agg)
    else:
        global_top_k_result = slice_agg()

    if aggregator == 'LSTM':
        global_top_k_result = tf.div(global_top_k_result, tf.cast(n, tf.float32))
        global_top_k_result = lstm_cell(global_top_k_result, name=scope + '/LSTM')
    elif aggregator == 'mean':
        global_top_k_result = tf.div(global_top_k_result, tf.cast(n, tf.float32))
    fea_v = tf.expand_dims(fea_v, axis=-1, name=scope + '/expand2')
    if mode == 'embedding':
        global_top_k_result = tf.concat([fea_v, global_top_k_result], axis=1, name=scope + '/concat')
    elif mode == 'conv':
        global_top_k_result = tf.concat([fea_v, global_top_k_result], axis=2, name=scope + '/concat')
    else:
        print('mode {} is not supported!'.format(mode))
    global_top_k_result = tf.transpose(global_top_k_result, perm=[0, 2, 1], name=scope+'/trans2')
    return global_top_k_result


def aggregator_edge(adj_m, fea_m, fea_e, k, scope, aggregator, n_slice, partitions, is_train, use_batch, mode='conv'):
    adj_m = tf.transpose(adj_m, perm=[1, 0])  # Ne x (Nv + Ne) -> (Nv + Ne) x Ne
    n = tf.shape(adj_m)[0]

    def subgraph_agg():
        # n_slice = 1
        return select_vertex_neighbors_slice(adj_m, fea_m, k, scope, aggregator)

    def slice_agg():
        return aggregate()

    def aggregate():
        adj_m_parts = tf.dynamic_partition(adj_m, partitions, n_slice)
        feat_m_parts = tf.dynamic_partition(fea_m, partitions, n_slice)

        # gather_idx = tf.range(slice_size)
        global_top_k_result = select_vertex_neighbors_slice(adj_m_parts[0], feat_m_parts[0],
                                                            k, scope, aggregator)

        for i in range(1, n_slice):
            sub_adj_m = adj_m_parts[i]
            sub_feat_m = feat_m_parts[i]
            local_top_k_result = select_edge_neighbors_slice(sub_adj_m, sub_feat_m, k, scope, aggregator)

            if aggregator == 'max_pooling':
                global_top_k_result = tf.nn.top_k(tf.concat([global_top_k_result, local_top_k_result], axis=-1),
                                                  k=k, name=scope + '/slice_top_k_edge').values
            elif aggregator in ('mean', 'LSTM'):
                global_top_k_result += local_top_k_result
        return global_top_k_result

    if use_batch:
        # if subgraph is used for training, slicing will not needed.
        global_top_k_result = tf.cond(is_train, subgraph_agg, slice_agg)
    else:
        global_top_k_result = slice_agg()

    if aggregator == 'LSTM':
        global_top_k_result = tf.div(global_top_k_result, tf.cast(n, tf.float32))
        global_top_k_result = lstm_cell(global_top_k_result, name=scope + '/LSTM')
    elif aggregator == 'mean':
        global_top_k_result = tf.div(global_top_k_result, tf.cast(n, tf.float32))
    fea_e = tf.expand_dims(fea_e, axis=-1, name=scope + '/expand_edge')
    if mode == 'embedding':
        global_top_k_result = tf.concat([global_top_k_result, fea_e], axis=1, name=scope + '/concat_edge')
    elif mode == 'conv':
        global_top_k_result = tf.concat([fea_e, global_top_k_result], axis=2, name=scope + '/concat_edge')
    else:
        print('mode {} is not supported!'.format(mode))
    # if fea_e is not None:

    global_top_k_result = tf.transpose(global_top_k_result, perm=[0, 2, 1], name=scope + '/trans2_edge')
    return global_top_k_result


def select_vertex_neighbors_slice(adj_m, fea_m, k, scope, aggregator):
    adj_m = tf.abs(adj_m, name=scope + '/abs')
    adj_m = tf.expand_dims(adj_m, axis=1, name=scope + '/expand1')
    fea_m = tf.expand_dims(fea_m, axis=-1, name=scope + '/expand2')

    feas = tf.multiply(adj_m, fea_m, name=scope + '/mul')  # extract all neighbor
    feas = tf.transpose(feas, perm=[2, 1, 0], name=scope + '/trans_vertex')

    if aggregator == 'max_pooling':
        neighbors = tf.nn.top_k(feas, k=k, name=scope + '/ngb_top_k').values
    elif aggregator in ('mean', 'LSTM'):
        neighbors = tf.reduce_sum(feas, axis=2, keepdims=True, name=scope + '/ngb_sum')
    return neighbors


def select_edge_neighbors_slice(adj_m, fea_m, k, scope, aggregator):
    adj_m = tf.abs(adj_m, name=scope + '/abs')
    adj_m = tf.transpose(adj_m, perm=[1, 0])
    fea_m = tf.transpose(fea_m, perm=[1, 0])
    fea_m = tf.expand_dims(fea_m, axis=1, name=scope + '/expand1')

    feas = tf.multiply(adj_m, fea_m, name=scope + '/mul')  # extract all neighbor
    feas = tf.transpose(feas, perm=[1, 0, 2], name=scope + '/trans1_edge')

    if aggregator == 'max_pooling':
        neighbors = tf.nn.top_k(feas, k=k, name=scope + '/ngb_top_k').values
    elif aggregator in ('mean', 'LSTM'):
        neighbors = tf.reduce_sum(feas, axis=2, keepdims=True, name=scope + '/ngb_sum')
    return neighbors


# Added by Yudi Chen - 20181211
# ****************Start ...

def lstm_cell(feas, name):
    '''
    feas: batch_size, time_step, sample_num
    '''
    hidden_dim = 8
    output_dim = int(feas.shape[2])
    #    LSTM layer
    cell = tf.contrib.rnn.BasicLSTMCell(hidden_dim, activation=tf.nn.relu)
    with tf.variable_scope(name) as scope:
        try:
            rnn_outputs, rnn_states = tf.nn.dynamic_rnn(cell, feas, dtype=tf.float32)
        except ValueError:
            scope.reuse_variables()
            rnn_outputs, rnn_states = tf.nn.dynamic_rnn(cell, feas, dtype=tf.float32)

    #    Linear layer
    weight = tf.Variable(tf.random_normal([hidden_dim, output_dim]))
    bias = tf.Variable(tf.random_normal([output_dim]))
    rnn_outputs = tf.reshape(rnn_outputs, [-1, hidden_dim])
    stacked_outputs = tf.matmul(rnn_outputs, weight) + bias
    stacked_outputs = tf.reshape(stacked_outputs, [-1, int(feas.shape[1]), output_dim])

    return stacked_outputs

# ****************End ...


def loss_cost(preds, labels, name='loss'):
    with tf.variable_scope(name):

        # preds = tf.sigmoid(preds)
        loss_cost = tf.losses.mean_squared_error(preds, labels)
        return loss_cost


def masked_accuracy(preds, labels, name='accuracy'):
    with tf.variable_scope(name):
        correct_prediction = tf.equal(tf.argmax(preds, 1), tf.argmax(labels, 1))
        accuracy_all = tf.cast(correct_prediction, tf.float32)
        return tf.reduce_mean(accuracy_all)


def multi_logit_pred(incoming_e, out_e):
    index = tf.argmax(incoming_e, axis=1)
    index, _ = tf.unique(index)

    adj_m = tf.gather(incoming_e, index, axis=1)
    out_e = tf.multiply(adj_m, out_e)
    out_e = tf.nn.softmax(out_e, axis=0)
    # set non-neighbor to 0
    out_e = tf.multiply(adj_m, out_e)

    # re-normalize
    out_e = tf.div_no_nan(out_e, tf.reduce_sum(out_e, axis=0))
    out_e = tf.reduce_sum(out_e, axis=1, keepdims=True, name='multi-logit-sum')
    tfprint = tf.print('@@@@multi-logit output ', out_e)
    with tf.control_dependencies([tfprint]):
        print(out_e)
    return out_e
