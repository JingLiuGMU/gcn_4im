import os
import argparse
import tensorflow as tf
from network import GraphNet

####Delete all flags before declare#####
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)


del_all_flags(tf.flags.FLAGS)

flags = tf.app.flags
flags.DEFINE_integer('max_step', 1000, '# of step for training')
flags.DEFINE_integer('summary_interval', 10, '# of step to save summary')
flags.DEFINE_float('learning_rate', 0.01, 'learning rate')
flags.DEFINE_boolean('is_train', True, 'is train')
flags.DEFINE_string('gpuid', '0', 'specify gpuid')
flags.DEFINE_integer('window_size', 4, 'window size')
flags.DEFINE_integer('stride', 2, 'stride')
flags.DEFINE_string('dataset', 'CVE', 'dataset')
flags.DEFINE_string('filename', './data/sentiment_incld/CVE/size_4_stride_2/CVE_input_bern_prob_span_', 'input file path and name')
flags.DEFINE_integer('spans', 4, 'spans numbers, the last span treated as test set')
flags.DEFINE_string('prob_name', 'BT', 'probability name')
# flags.DEFINE_integer('nfeat', 2, 'number of vertex features for training')
flags.DEFINE_string('init_pool', 'max', 'pooling for init embedding vertex feature')
# Debug
flags.DEFINE_string('logdir', './logdir', 'Log dir')
flags.DEFINE_string('modeldir', './modeldir', 'Model dir')
flags.DEFINE_string('model_name', 'model', 'Model file name')
flags.DEFINE_integer('reload_step', 0, 'Reload step to continue training')
flags.DEFINE_integer('test_step', 0, 'Test or predict model at this step')
flags.DEFINE_boolean('self_loop', False, 'aggregation including the center vertex')
# network architecture
flags.DEFINE_integer('ch_num', 4, 'channel number')
flags.DEFINE_integer('layer_num', 2, 'block number')
flags.DEFINE_integer('fc_layer_num', 1, 'fc layer after LGL')
flags.DEFINE_float('adj_keep_r', 1, 'dropout keep rate')
flags.DEFINE_float('keep_r', 0.2, 'dropout keep rate')
flags.DEFINE_float('weight_decay', 5e-4, 'Weight for L2 loss on embedding matrix.')
flags.DEFINE_integer('k', 2, 'top k')
flags.DEFINE_string('first_conv', 'simple_conv', 'simple_conv, chan_conv')
flags.DEFINE_string('second_conv', 'graph_conv', 'graph_conv, simple_conv')
flags.DEFINE_boolean('use_batch', False, 'use batch training')
flags.DEFINE_integer('batch_size', 2500, 'batch size number')
flags.DEFINE_integer('center_num', 1500, 'start center number')
flags.DEFINE_string('exp_name', '', 'experiment name: e.g., {model}_{netG}_size{loadSize}')
flags.DEFINE_string('reg_model', 'sigmoid', 'regression model: sigmoid/beta regression/tanh/multi_logit')
# fix bug of flags
flags.FLAGS.__dict__['__parsed'] = False



def main(_):
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    model = GraphNet(tf.Session(config=config), flags.FLAGS)
    model.train()
    #GraphNet(tf.Session(), conf).train()


if __name__ == '__main__':
    # configure which gpu or cpu to use
    os.environ['CUDA_VISIBLE_DEVICES'] = flags.FLAGS.gpuid
    tf.app.run()
