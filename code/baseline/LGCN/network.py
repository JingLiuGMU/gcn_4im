import os
import time
import random
import numpy as np
import networkx as nx
import tensorflow as tf
import ops
import matplotlib.pyplot as plt
from utils import load_data, preprocess_features, preprocess_adj, gen_in_out_e_adj, cal_evl_metric
from batch_utils import get_sampled_index, get_indice_graph
import kdd_ops


class GraphNet(object):

    def __init__(self, sess, conf):
        self.sess = sess
        self.conf = conf
        if conf.exp_name:
            exp_name = (conf.exp_name.format(**(conf.flag_values_dict()))) if conf.exp_name != '' else ''
            exp_name = exp_name.replace(', ', '+')
            exp_name = exp_name.replace('[', '')
            exp_name = exp_name.replace(']', '')
            conf.modeldir = conf.modeldir + exp_name + '/'
            conf.logdir = conf.logdir + exp_name + '/'

        if not os.path.exists(conf.modeldir):
            os.makedirs(conf.modeldir)
        if not os.path.exists(conf.logdir):
            os.makedirs(conf.logdir)
        self.process_data()
        self.configure_networks()
        self.train_summary = self.config_summary('train')
        self.valid_summary = self.config_summary('valid')
        self.test_summary = self.config_summary('test')

    def inference(self, outs):
        inputs = outs
        outs = getattr(ops, self.conf.first_conv)(
            self.normed_matrix, outs, 4*self.conf.ch_num, self.conf.adj_keep_r,
            self.conf.keep_r, self.is_train, 'conv_s', act_fn=None)
        for layer_index in range(self.conf.layer_num):
            cur_outs= getattr(ops, self.conf.second_conv)(
                self.normed_matrix, outs, self.conf.ch_num, self.conf.adj_keep_r,
                self.conf.keep_r, self.is_train, 'conv_%s' % (layer_index+1),
                act_fn=None, k=self.conf.k)
            outs = tf.concat([outs, cur_outs], axis=1, name='concat_%s' % layer_index)
        outs = tf.concat([outs, inputs], axis=1)
        incoming_v = tf.matmul(self.incoming_e, outs, name='incoming_v_feat')
        outgoing_v = tf.matmul(self.outgoing_e, outs, name='outgoing_v_feat')
        out_e = tf.concat([outgoing_v, incoming_v], axis=1, name='concat_vertex_edge_feat')

        # multiple fc layers
        for layer_index in range(self.conf.fc_layer_num):
            num_out = out_e.shape[-1].value
            num_out = num_out // 2 + 1
            out_e = kdd_ops.linear(
                out_e, num_out,
                1.0, self.is_train, 'linear_output_l%s' % layer_index, act_fn=tf.nn.relu)

        if self.conf.reg_model == 'sigmoid':
            out_e = kdd_ops.linear(
                out_e, 1,
                1.0, self.is_train, 'linear_output', act_fn=tf.nn.sigmoid)
        elif self.conf.reg_model == 'tanh':
            out_e = kdd_ops.linear(
                out_e, 1,
                1.0, self.is_train, 'linear_output', act_fn=tf.nn.tanh)
            out_e = tf.abs(out_e)
        elif self.conf.reg_model == 'multi_logit':
            out_e = kdd_ops.linear(
                out_e, 1,
                1.0, self.is_train, 'linear_output', act_fn=None)  # w x he get Ne x 1

            out_e = kdd_ops.multi_logit_pred(self.incoming_e, out_e)

        else:
            raise NotImplementedError('{} regression model Not support!'.format(self.conf.reg_model))

        # outs = ops.simple_conv(
        #     self.normed_matrix, out_e, self.conf.class_num, self.conf.adj_keep_r,
        #     self.conf.keep_r, self.is_train, 'conv_f', act_fn=None, norm=False)
        return out_e

    def get_optimizer(self, lr):
        return tf.contrib.opt.NadamOptimizer(lr)

    def process_data(self):
        '''
                Organize the data into training, testing datasets
                For each dataset:
                                vertex, edge&vertex, and edge&edge are considered
                                please refer to kdd_utils for detailed function'''
        print('process input data...')
        data = load_data(self.conf.filename, self.conf.spans, self.conf.init_pool)
        # training dataset
        adj_v, adj_e = data[:2]
        print('no. of vertex: {}, no. of edge: {}'.format(adj_v[0].shape[0], adj_e[0].shape[0]))
        # self.adj_v_train, self.adj_e_train = adj_v, adj_e
        self.adj_train = adj_v
        self.normed_adj_train = preprocess_adj(adj_v, self.conf.self_loop)
        self.normed_adj_ev_train = adj_e  # preprocess_adj(adj_e, False)
        self.adj_incoming_e_train, self.adj_outgoing_e_train = gen_in_out_e_adj(adj_e)

        self.feas_train  = preprocess_features(data[2], False)
        self.labels_train = data[3]
        # self.parts_v_mask = get_partition_mask(adj_v, self.conf.n_slice)
        # self.parts_e_mask = get_partition_mask(adj_e, self.conf.n_slice)
        # self.parts_ev_mask = get_partition_mask(adj_v, self.conf.n_slice, adj_e)

        # testing dataset
        adj_v, adj_e = data[4:6]
        self.adj_test = adj_v
        self.adj_incoming_e_test, self.adj_outgoing_e_test = gen_in_out_e_adj(adj_e)
        # self.adj_v_test, self.adj_e_test = adj_v, adj_e
        self.normed_adj_test = preprocess_adj(adj_v, self.conf.self_loop)
        self.normed_adj_ev_test = adj_e  # preprocess_adj(adj_e, False)
        self.feas_test = preprocess_features(data[6], False)
        self.labels_test = data[7]
        # self.parts_v_mask_test = get_partition_mask(adj_v, self.conf.n_slice)
        # self.parts_e_mask_test = get_partition_mask(adj_e, self.conf.n_slice)
        # self.parts_ev_mask_test = get_partition_mask(adj_v, self.conf.n_slice, adj_e)

    def configure_networks(self):
        self.build_network()
        self.cal_loss()
        optimizer = self.get_optimizer(self.conf.learning_rate)
        self.train_op = optimizer.minimize(self.loss_op, name='train_op')
        self.seed = int(time.time())
        tf.set_random_seed(self.seed)
        self.sess.run(tf.global_variables_initializer())
        trainable_vars = tf.trainable_variables()
        self.saver = tf.train.Saver(var_list=trainable_vars, max_to_keep=0)
        if self.conf.is_train:
            self.writer = tf.summary.FileWriter(self.conf.logdir, self.sess.graph)
        self.print_params_num()

    def build_network(self):
        self.labels_mask = tf.placeholder(tf.int32, None, name='labels_mask')  # masking labels for MR subgraph
        self.matrix = tf.placeholder(tf.int32, [None, None], name='matrix')
        self.normed_matrix = tf.placeholder(tf.float32, [None, None], name='normed_matrix')
        self.inputs = tf.placeholder(tf.float32, [None, self.feas_train[0].shape[1]], name='inputs')
        self.labels = tf.placeholder(tf.float32, [None], name='labels')  # ground-truth of probability
        self.is_train = tf.placeholder(tf.bool, name='is_train')

        self.adj_edge = tf.placeholder(tf.float32, [None, None])  # Ne x Ne
        self.incoming_e = tf.placeholder(tf.float32, [None, None])  # Ne x Nv
        self.outgoing_e = tf.placeholder(tf.float32, [None, None])  # Ne x Nv
        self.normed_adj_ev_matrix = tf.placeholder(tf.float32, [None, None], name='normed_e_matrix')  # Ne x Nv
        self.preds = self.inference(self.inputs)

    def cal_loss(self):
        with tf.variable_scope('loss'):
            self.preds = tf.squeeze(self.preds, axis=1)
            if self.conf.use_batch and (self.conf.reg_model == 'multi_logit'):
                self.label_mask = tf.cast(self.label_mask, tf.float32)
                self.preds = tf.multiply(self.preds, self.label_mask)
                self.labels = tf.multiply(self.labels, self.label_mask)
            self.mse = kdd_ops.loss_cost(
                self.preds, self.labels)
            # add l2 regularization to linear output layers
            self.regu_loss = 0
            for var in tf.trainable_variables():
                if all(word in var.name for word in ['weight', 'linear_output']):
                    self.regu_loss += self.conf.weight_decay * tf.nn.l2_loss(var) / var.shape.num_elements()
            self.loss_op = self.mse + self.regu_loss

    def config_summary(self, name):
        summarys = []
        summarys.append(tf.summary.scalar(name+'/loss', self.loss_op))
        #summarys.append(tf.summary.scalar(name+'/class_loss', self.class_loss))
        if name == 'train':
            summarys.append(tf.summary.scalar(name+'/regu_loss', self.regu_loss))
        summary = tf.summary.merge(summarys)
        return summary

    def save_summary(self, summary, step):
        self.writer.add_summary(summary, step)

    def test(self):
        # self.reload(self.conf.load_step)
        fig_name = self.conf.modeldir + 'pred_true_test_latest.png'
        if self.conf.load_step is not None:
            checkpoint_path = os.path.join(
                self.conf.modeldir, self.conf.model_name)
            model_path = checkpoint_path + '-' + self.conf.load_step
            print('ckpt path:{}'.format(model_path))
            self.saver.restore(self.sess, model_path)
            fig_name = self.conf.modeldir + 'pred_true_test_' + str(self.conf.load_step) + '.png'
        else:
            if not os.path.exists(os.path.join(self.conf.modeldir, "latest.ckpt") + '.meta'):
                print('------- no such checkpoint', os.path.join(self.conf.modeldir, "latest.ckpt"))
                return
            print('ckpt path:{}'.format(self.conf.modeldir))
            self.saver.restore(self.sess, os.path.join(self.conf.modeldir, "latest.ckpt"))

        feed_test_dict = self.pack_trans_dict('test', 0)
        pred, label = self.sess.run([self.preds, self.labels], feed_dict=feed_test_dict)
        pred = np.squeeze(pred)
        np.savetxt(self.conf.modeldir + "pred_true_test.csv", np.c_[pred, label], delimiter=",")
        metric = cal_evl_metric(pred, label)
        print('metric: {}'.format(metric))
        self.write_to_file(metric)
        x = np.linspace(0.0, 1.0, num=10)
        y = x
        plt.plot(x, y)
        plt.title('{}_wsize{}_stride{}_{}'.format(self.conf.dataset, self.conf.window_size,
                                                  self.conf.stride, self.conf.aggregator))
        plt.plot(label, pred, '.', label='test')
        plt.legend(loc=4)
        # plt.show()
        plt.savefig(fig_name)
        plt.clf()

    def train(self):
        if self.conf.reload_step > 0:
            self.reload(self.conf.reload_step)
        self.transductive_train()

    def transductive_train(self):
        # feed_train_dict = self.pack_trans_dict('train')
        # feed_valid_dict = self.pack_trans_dict('valid')
        feed_test_dict = self.pack_trans_dict('test', 0)
        for epoch_num in range(self.conf.max_step+1):
            for batch_index in range(self.conf.spans - 1):
                feed_train_dict = self.pack_trans_dict('train', batch_index)
                train_loss, _, summary, preds = self.sess.run(
                    [self.loss_op, self.train_op, self.train_summary, self.preds],
                    feed_dict=feed_train_dict)
                self.save_summary(summary, epoch_num+self.conf.reload_step)
                # summary= self.sess.run(
                #     [self.valid_summary],
                #     feed_dict=feed_valid_dict)
                # self.save_summary(summary, epoch_num+self.conf.reload_step)
                summary, test_loss = self.sess.run(
                    [self.test_summary, self.loss_op],
                    feed_dict=feed_test_dict)
                self.save_summary(summary, epoch_num+self.conf.reload_step)

                if epoch_num and epoch_num % 100 == 0:
                    self.save(epoch_num)
                print('step: %d --- train loss: %.4f, test loss:  %.4f' %(
                    epoch_num, train_loss, test_loss))
                print(preds.shape)

        test_values = self.sess.run([self.preds, self.labels], feed_dict=feed_test_dict)
        print('preds ', test_values[0])

        plt.plot(test_values[1], test_values[0], '.', label='test')
        x = np.linspace(0.0, 1.0, num=10)
        y = x
        plt.plot(x, y)
        plt.legend(loc=4)
        # plt.show()
        plt.savefig(self.conf.modeldir + 'pred_true.png')
        plt.clf()
        np.savetxt(self.conf.modeldir + "pred_true.csv", np.c_[test_values[0], test_values[1]], delimiter=",")
        metric = cal_evl_metric(test_values[0], test_values[1])
        print('metric: {}'.format(metric))
        self.write_to_file(metric)

    def write_to_file(self, metrics):
        import csv
        filename = './data/baseline/LGCN.csv'
        if not os.path.isfile(filename):
            writer = csv.writer(open(filename, 'w+', newline=''))
            res_summary = []
            res_summary.extend(['dataset', 'prob_name', 'window_size', 'stride', 'r2', 'mae', 'mse', 'mape'])
            writer.writerow(res_summary)
        else:
            writer = csv.writer(open(filename, 'a', newline=''))

        res_summary = []

        res_summary.extend([self.conf.dataset, self.conf.prob_name, self.conf.window_size, self.conf.stride,
                            metrics[0], metrics[1], metrics[2], metrics[3]])

        writer.writerow(res_summary)
        
    def pack_trans_dict(self, action, index):
        feed_dict = {self.matrix: self.adj_train[index], self.normed_matrix: self.normed_adj_train[index],
                self.inputs: self.feas_train[index], self.normed_adj_ev_matrix: self.normed_adj_ev_train[index],
                self.incoming_e: self.adj_incoming_e_train[index],
                self.outgoing_e: self.adj_outgoing_e_train[index],
                self.labels: self.labels_train[index]}
        if action == 'train':
            feed_dict.update({self.is_train: True})
            if self.conf.use_batch:
                v_indice, e_indice, mask = get_sampled_index(self.normed_adj_train[index],
                                                             self.normed_adj_ev_train[index],
                                                             self.conf.batch_size, self.conf.center_num)
                feed_dict.update({
                    self.incoming_e: self.adj_incoming_e_train[index][e_indice, :][:, v_indice],
                    self.outgoing_e: self.adj_outgoing_e_train[index][e_indice, :][:, v_indice],
                    self.inputs: self.feas_train[index][v_indice],
                    self.labels: self.labels_train[index][e_indice],
                    self.matrix: self.adj_train[index][v_indice, :][:, v_indice],
                    self.normed_matrix: self.normed_adj_train[index][v_indice, :][:, v_indice],
                    self.normed_adj_ev_matrix: self.normed_adj_ev_train[index][e_indice, :][:, v_indice],
                    self.labels_mask: mask})
        elif action == 'valid':
            feed_dict.update({self.is_train: False})
            if self.conf.use_batch:
                mask = np.ones_like(self.labels_train[index])
                feed_dict.update({self.labels_mask: mask})
        else:
            feed_dict.update({
                self.matrix: self.adj_test[index], self.normed_matrix: self.normed_adj_test[index],
                self.inputs: self.feas_test[index], self.normed_adj_ev_matrix: self.normed_adj_ev_test[index],
                self.incoming_e: self.adj_incoming_e_test[index],
                self.outgoing_e: self.adj_outgoing_e_test[index],
                self.labels: self.labels_test[index],
                self.is_train: False})
            if self.conf.use_batch:
                mask = np.ones_like(self.labels_test[index])
                feed_dict.update({self.labels_mask: mask})
        return feed_dict

    def save(self, step):
        print('---->saving', step)
        checkpoint_path = os.path.join(
            self.conf.modeldir, self.conf.model_name)
        self.saver.save(self.sess, checkpoint_path, global_step=step)

    def reload(self, step):
        checkpoint_path = os.path.join(
            self.conf.modeldir, self.conf.model_name)
        model_path = checkpoint_path+'-'+str(step)
        if not os.path.exists(model_path+'.meta'):
            print('------- no such checkpoint', model_path)
            return
        self.saver.restore(self.sess, model_path)

    def print_params_num(self):
        total_params = 0
        for var in tf.trainable_variables():
            print(var)
            total_params += var.shape.num_elements()
        print("The total number of params --------->", total_params)
