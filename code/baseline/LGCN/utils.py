import os
import sys
import json
import numpy as np
import pickle as pkl
import networkx as nx
import scipy.sparse as sp
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
from scipy.sparse.linalg.eigen.arpack import eigsh
from networkx.readwrite import json_graph


def parse_index_file(filename):
    """Parse index file."""
    index = []
    for line in open(filename):
        index.append(int(line.strip()))
    return index


def sample_mask(idx, l):
    """Create mask."""
    mask = np.zeros(l)
    mask[idx] = 1
    return np.array(mask, dtype=np.bool)


def load_data(dataset_str, batches, pool):
    return load_small_data(dataset_str, batches, pool)


def load_small_data(dataset_str, batches, pool):
    """Load data."""
    """Load data."""
    adjs_v_train = []
    adjs_e_train = []
    feats_v_train = []
    labels_train = []
    adjs_v_test = []
    adjs_e_test = []
    feats_v_test = []
    labels_test = []
    for i in range(batches):
        filename = dataset_str + str(i) + '.npz'
        inputs = np.load(filename)
        if i < (batches - 1):
            adjs_v_train.append(inputs['Av'])
            adjs_e_train.append(inputs['Ae'])
            temp_feat = init_embed_vertex(inputs['fv'], inputs['fe'], inputs['Ae'], pool)
            feats_v_train.append(temp_feat)
            labels_train.append(inputs['label'])
        elif i == (batches - 1):
            adjs_v_test.append(inputs['Av'])
            adjs_e_test.append(inputs['Ae'])
            temp_feat = init_embed_vertex(inputs['fv'], inputs['fe'], inputs['Ae'], pool)
            feats_v_test.append(temp_feat)
            labels_test.append(inputs['label'])
    print('feat dim', temp_feat.shape[-1])
    return adjs_v_train, adjs_e_train, feats_v_train, labels_train, adjs_v_test, adjs_e_test, feats_v_test, labels_test


def init_embed_vertex(fv, fe, Ae, pool):
    Ae[Ae == -1] = 0
    Ae = np.expand_dims(Ae, axis=1)

    fe = np.expand_dims(fe, axis=-1)
    feat = np.multiply(Ae, fe)

    feat = np.transpose(feat, (2, 1, 0))
    if 'max' in pool:
        feat = np.max(feat, axis=-1)
    elif 'mean' in pool:
        feat = np.mean(feat, axis=-1)
    else:
        raise TypeError('{} pool is not supported!'.format(pool))
    feat = np.concatenate([fv, feat], axis=1)
    return np.squeeze(feat)


def sparse_to_tuple(sparse_mx):
    """Convert sparse matrix to tuple representation."""
    def to_tuple(mx):
        if not sp.isspmatrix_coo(mx):
            mx = mx.tocoo()
        coords = np.vstack((mx.row, mx.col)).transpose()
        values = mx.data
        shape = mx.shape
        return coords, values, shape

    if isinstance(sparse_mx, list):
        for i in range(len(sparse_mx)):
            sparse_mx[i] = to_tuple(sparse_mx[i])
    else:
        sparse_mx = to_tuple(sparse_mx)

    return sparse_mx


# def preprocess_features(features, sparse=True):
#     """Row-normalize feature matrix and convert to tuple representation"""
#     rowsum = np.array(features.sum(0))
#     #import ipdb; ipdb.set_trace()
#     try:
#         # r_inv = np.power(rowsum, -1).flatten()
#         # r_inv[np.isinf(r_inv)] = 0.
#         # r_mat_inv = sp.diags(r_inv)
#         # features = r_mat_inv.dot(features)
#
#         r_inv = np.power(rowsum, -1).flatten()
#         r_inv[np.isinf(r_inv)] = 0.
#         r_mat_inv = sp.csr_matrix(r_inv)
#         features = r_mat_inv.T.multiply(features.T).T
#     except Exception as e:
#         print(e)
#
#     if sparse:
#         return sparse_to_tuple(features)
#     return features.todense()

def preprocess_features(features, sparse=False):
    """Row-normalize feature matrix and convert to tuple representation"""
    for i in range(len(features)):

        colsum = np.array(features[i].sum(0))
    # import ipdb; ipdb.set_trace()
        try:
            r_inv = np.power(colsum, -1).flatten()
            r_inv[np.isinf(r_inv)] = 0.
            r_mat_inv = sp.csr_matrix(r_inv)
            features[i] = r_mat_inv.T.multiply(features[i].T).T.todense()
        except Exception as e:
            print(e)

    if sparse:
        features[i] = sparse_to_tuple(features[i])

    return features


def normalize_adj(adj):
    """Symmetrically normalize adjacency matrix."""
    adj = sp.coo_matrix(adj)
    rowsum = np.array(adj.sum(1))
    d_inv_sqrt = np.power(rowsum, -1).flatten()
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = sp.csr_matrix(d_inv_sqrt)
    return d_mat_inv_sqrt.T.multiply(adj).tocoo()


def preprocess_adj(adj, self_loop=False, norm=True, sparse=False):
    """Preprocessing of adjacency matrix for simple GCN model and conversion to tuple representation."""
    for i in range(len(adj)):
        # adj[i] = np.abs(adj[i])
        if self_loop:
            # adj[i] = adj[i] + sp.eye(adj[i].shape[0], adj[i].shape[1])
            np.fill_diagonal(adj[i], 1)
        if norm:
            adj[i] = normalize_adj(adj[i]).todense()
        if sparse:
            adj[i] = sparse_to_tuple(adj[i])
        # adj[i] = adj[i].todense()
    return adj


def construct_feed_dict(features, support, labels, labels_mask, is_training, placeholders):
    """Construct feed dictionary."""
    feed_dict = {
        placeholders['labels']: labels,
        placeholders['labels_mask']: labels_mask,
        placeholders['features']: features,
        placeholders['support']: support,
        placeholders['num_features_nonzero']: features.shape,
        placeholders['is_training']: is_training}
    return feed_dict


def gen_in_out_e_adj(adj, norm=True):
    incoming_adj = []
    outgoing_adj = []
    for i in range(len(adj)):
        adj_incoming_e = np.copy(adj[i])
        adj_incoming_e[adj_incoming_e == -1] = 0
        incoming_adj.append(adj_incoming_e)
        adj_outgoing_e = np.copy(adj[i])
        adj_outgoing_e[adj_outgoing_e == 1] = 0
        outgoing_adj.append(np.abs(adj_outgoing_e))
    # incoming_adj = preprocess_adj(incoming_adj)
    # outgoing_adj = preprocess_adj(outgoing_adj)
    return incoming_adj, outgoing_adj


def cal_evl_metric(pred, label, remove=False):
    if remove:
        pred = pred[np.where(pred > 0.)]
        label = label[np.where(pred > 0.)]
    r2 = r2_score(label, pred)
    mae = mean_absolute_error(label, pred)
    mse = mean_squared_error(label, pred)
    mape = mean_absolute_percentage_error(label, pred)
    return r2, mae, mse, mape


def mean_absolute_percentage_error(y_true, y_pred):
    predi = y_pred[np.where(y_true > 1e-5)]
    labeli = y_true[np.where(y_true > 1e-5)]
    return np.mean(np.abs(labeli - predi) / labeli)


if __name__ == "__main__":
    load_data('./data/sentiment_incld/CVE/size_3_stride_3/CVE_input_jaccard_prob_span_', 3, 'max')