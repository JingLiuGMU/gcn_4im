import argparse, os, csv
import pandas as pd
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
import numpy as np
import matplotlib.pyplot as plt


def baseline(args):
    args.dir += args.dataset + '/'
    args.dir += "size_" + str(args.window_size) + "_stride_" + str(args.stride) + "_t-" + str(args.threshold) + "/"
    data = load_data(args.dir, args.dataset, args.span, args.prob_name)
    # plt.plot(data.iloc[:, -2], data.iloc[:, -1], 'o')
    # plt.show()
    r2, mae, mse, mape = cal_evl_metric(data.iloc[:, -2], data.iloc[:, -1])

    save_metrics(args, r2, mae, mse, mape)
    print("r2: {} mae: {} mse:{} mape: {}".format(r2, mae, mse, mape))


def save_metrics(args, r2, mae, mse, mape):
    if not os.path.isfile(args.output):
        writer = csv.writer(open(args.output, 'w+', newline=''))
        res_summary = ['dataset','window_size', 'stride', 'threshold', 'prob_name',  'r2', 'mae', 'mse', 'mape']
        writer.writerow(res_summary)

    writer = csv.writer(open(args.output, 'a', newline=''))
    res_summary = [args.dataset, args.window_size, args.stride, args.threshold, args.prob_name, r2, mae, mse, mape]
    writer.writerow(res_summary)


def load_data(dir, dataset, span, prob_name):
    if prob_name == "BT":
        suffix = "_input_bern_prob_span_"
    elif prob_name == "JI":
        suffix = "_input_jaccard_prob_span_"
    elif prob_name == "LT":
        suffix = "_input_lt_prob_span_"
    else:
        raise NotImplementedError('prob name not implemented!')

    filename = dir + dataset + suffix + str(span - 1) + ".csv"
    data = pd.read_csv(filename)
    return data


def cal_evl_metric(pred, label):
    pred = np.squeeze(pred)
    r2 = r2_score(label, pred)
    mae = mean_absolute_error(label, pred)
    mse = mean_squared_error(label, pred)
    mape = mean_absolute_percentage_error(label, pred)
    return r2, mae, mse, mape


def mean_absolute_percentage_error(y_true, y_pred):
    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)
    predi = y_pred[np.where(y_true > 1e-15)]
    labeli = y_true[np.where(y_true > 1e-15)]
    return np.mean(np.abs(labeli - predi) / labeli)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='data preparing.')
    parser.add_argument('--dir', '-dir', type=str, default='./data/', help='path to data file')
    parser.add_argument('--dataset', '-d', type=str, default='CVE', help='dataset to be processed')
    parser.add_argument('--span', '-sp', type=int, default=4, help='# of spans')
    parser.add_argument('--stride', '-s', type=int, default=2, help='stride between two time windows')
    parser.add_argument('--threshold', '-t', type=int, default=20)
    parser.add_argument('--window_size', '-w', type=int, default=4, help='sliding window size')
    parser.add_argument('--prob_name', '-pn', type=str, default='BT', help='specify the prob name')
    parser.add_argument('--output', '-o', type=str, default='./data/pred_baseline.csv', help='specify the prob name')

    args = parser.parse_args()
    baseline(args)