import plotly.plotly as py
import plotly.graph_objs as go

import networkx as nx
import pandas as pd
import numpy as np
inf_prob_df = pd.DataFrame()
import matplotlib.pyplot as plt
for suffix in ('robust', 'prime', 'true'):
    inf_prob = np.load('D:/gcn_4im/data/sentiment_incld/CVE/size_3_stride_3/graph/BT/influ_prob_' + suffix + '.npy')
    inf_prob_df[suffix] = pd.Series(inf_prob)

inf_prob_df['ID'] = inf_prob_df.index
graph_df = pd.read_csv('D:/gcn_4im/data/sentiment_incld/CVE/size_3_stride_3/graph/BT/graph_df.csv')

G = nx.from_pandas_edgelist(graph_df, 'from', 'to', create_using=nx.Graph())


# outdeg = G.degree()
# to_remove = [n for n in outdeg if outdeg[n] == 1]
# G.remove_nodes_from(to_remove)
for component in list(nx.connected_components(G)):
    if len(component)<40:
        for node in component:
            G.remove_node(node)
carac= inf_prob_df.set_index('ID')
carac=carac.reindex(G.nodes())
pos=nx.spring_layout(G)
#nx.draw(G, pos, with_labels=False, node_color=carac['average'], node_size=20, cmap=plt.cm.Blues_r)
plt.subplot(133)
node_size = 100
color = plt.cm.jet_r
mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['robust'], node_shape='o',
                             node_size=node_size*carac['robust'], cmap=color)
nx.draw_networkx_edges(G, pos)
ax= plt.gca()
ax.collections[0].set_edgecolor("#000000")

#plt.subplots_adjust(right=1)
plt.title('Influence Spread (robust)')
#plt.axis('off')
plt.xticks([], [])
plt.yticks([], [])
axes = plt.gcf().get_axes()
plt.colorbar(mcp)
plt.subplot(132)
mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['prime'], node_shape='o',
                             node_size=node_size*carac['prime'], cmap=color)
nx.draw_networkx_edges(G, pos)
ax= plt.gca()
ax.collections[0].set_edgecolor("#000000")
plt.colorbar(mcp)
plt.title('Influence Spread (ours)')
plt.xticks([], [])
plt.yticks([], [])
#plt.axis('off')
plt.subplot(131)
mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['true'], node_shape='o',
                             node_size=node_size*carac['true'], cmap=color)
nx.draw_networkx_edges(G, pos)
ax= plt.gca()
ax.collections[0].set_edgecolor("#000000")
plt.colorbar(mcp)
plt.xticks([], [])
plt.yticks([], [])
plt.title('Influence Spread (ground-truth)')
#plt.axis('off')
plt.show()
print()



