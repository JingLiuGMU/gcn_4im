import networkx as nx
import pandas as pd
import numpy as np
inf_prob_df = pd.DataFrame()
import matplotlib.pyplot as plt
prob_name = 'bern'
k = 5
suffix_list = ['ours_{}_{}'.format(prob_name, k), 'robust_{}_{}'.format(prob_name, k),
               'true_{}_{}'.format(prob_name, k)]
for suffix in suffix_list:
    inf_prob = np.load('data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/result_IM4TrueG_' + suffix + '.npy')
    inf_prob_df[suffix] = pd.Series(inf_prob)

inf_prob_df['ID'] = inf_prob_df.index
graph_df = pd.read_csv('data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/graph_df.csv')

G = nx.from_pandas_dataframe(graph_df, 'from', 'to', create_using=nx.Graph())


# outdeg = G.degree()
# to_remove = [n for n in outdeg if outdeg[n] == 1]
# G.remove_nodes_from(to_remove)
for component in list(nx.connected_components(G)):
    if len(component)<40:
        for node in component:
            G.remove_node(node)
carac= inf_prob_df.set_index('ID')
carac=carac.reindex(G.nodes())
pos=nx.spring_layout(G)

fig, (ax1, ax2, ax3) = plt.subplots(nrows=1, ncols=3, constrained_layout=True)
node_size = 120
color = plt.cm.jet

mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['true_{}_{}'.format(prob_name, k)], node_shape='o',
                             node_size=node_size*carac['true_{}_{}'.format(prob_name, k)], cmap=color, ax=ax1)
nx.draw_networkx_edges(G, pos, ax=ax1)

#ax1.set_edgecolor("#000000")
ax1.set_title('Influence Spread (optimal)', fontsize=20)
    # plt.axis('off')
ax1.set_xticks([], [])
ax1.set_yticks([], [])


mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['ours_{}_{}'.format(prob_name, k)], node_shape='o',
                             node_size=node_size*carac['ours_{}_{}'.format(prob_name, k)], cmap=color, ax=ax2)
nx.draw_networkx_edges(G, pos, ax=ax2)

#ax1.set_edgecolor("#000000")
ax2.set_title('Influence Spread (ours)', fontsize=20)
    # plt.axis('off')
ax2.set_xticks([], [])
ax2.set_yticks([], [])

mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['robust_{}_{}'.format(prob_name, k)], node_shape='o',
                             node_size=node_size*carac['robust_{}_{}'.format(prob_name, k)], cmap=color, ax=ax3)
nx.draw_networkx_edges(G, pos, ax=ax3)

#ax1.set_edgecolor("#000000")
ax3.set_title('Influence Spread (robust)', fontsize=20)
    # plt.axis('off')
ax3.set_xticks([], [])
ax3.set_yticks([], [])
# for ax in axes.flat:
#
#     mcp = nx.draw_networkx_nodes(G, pos, node_color=carac['robust'], node_shape='o',
#                                  node_size=node_size * carac['robust'], cmap=color)
#     nx.draw_networkx_edges(G, pos)
#     # ax= plt.gca()
#     # ax.collections[0].set_edgecolor("#000000")
#
#     #plt.subplots_adjust(right=1)
#     #plt.colorbar(mcp)
#     ax.set_title('Influence Spread (robust)')
#     # plt.axis('off')
#     ax.set_xticks([], [])
#     ax.set_yticks([], [])

#fig.subplots_adjust(right=0.8)
# put colorbar at desire position
#cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
cbar = fig.colorbar(mcp, ax=[ax1, ax2, ax3], orientation='horizontal')
cbar.ax.tick_params(labelsize=19)
fig.set_edgecolor("#000000")

plt.show()