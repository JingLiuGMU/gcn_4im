import networkx as nx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
data = pd.read_csv('data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/soc-redditHyperlinks-body_input_bern_prob_span_3.csv')
#G = nx.from_pandas_edgelist(data, 's_user', 'd_user', create_using=nx.Graph())
G = nx.from_pandas_dataframe(data, 'user_id_s', 'user_id_d', create_using=nx.Graph())
pos=nx.spring_layout(G)
nx.draw_networkx_nodes(G, pos, node_shape='o', node_size=10)
nx.draw_networkx_edges(G, pos)

plt.show()


