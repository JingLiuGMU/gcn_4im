#!/usr/bin/env bash


for pool in max mean
do

for data in cybersecurity ransomware
do

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_6_stride_1/${data}_input_lt_prob_span_ -sp 5 -o ./data/sentiment_incld/${data}/size_6_stride_1/graphsage/ -pn LT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_6_stride_1/${data}_input_bern_prob_span_ -sp 5 -o ./data/sentiment_incld/${data}/size_6_stride_1/graphsage/ -pn BT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_6_stride_1/${data}_input_jaccard_prob_span_ -sp 5 -o ./data/sentiment_incld/${data}/size_6_stride_1/graphsage/ -pn JI -pool ${pool}

done
done


for pool in max mean
do

for data in ransomware
do

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_5_stride_2/${data}_input_lt_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_5_stride_2/graphsage/ -pn LT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_5_stride_2/${data}_input_bern_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_5_stride_2/graphsage/ -pn BT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_5_stride_2/${data}_input_jaccard_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_5_stride_2/graphsage/ -pn JI -pool ${pool}

done
done

for pool in max mean
do

for data in CVE
do

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_6_stride_2/${data}_input_lt_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_6_stride_2/graphsage/ -pn LT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_6_stride_2/${data}_input_bern_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_6_stride_2/graphsage/ -pn BT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_6_stride_2/${data}_input_jaccard_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_6_stride_2/graphsage/ -pn JI -pool ${pool}

done
done




for pool in max mean
do

for data in CVE
do

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_3_stride_3/${data}_input_lt_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_3_stride_3/graphsage/ -pn LT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_3_stride_3/${data}_input_bern_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_3_stride_3/graphsage/ -pn BT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_3_stride_3/${data}_input_jaccard_prob_span_ -sp 3 -o ./data/sentiment_incld/${data}/size_3_stride_3/graphsage/ -pn JI -pool ${pool}

done
done


for pool in max mean
do

for data in CVE
do

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_4_stride_2/${data}_input_lt_prob_span_ -sp 4 -o ./data/sentiment_incld/${data}/size_4_stride_2/graphsage/ -pn LT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_4_stride_2/${data}_input_bern_prob_span_ -sp 4 -o ./data/sentiment_incld/${data}/size_4_stride_2/graphsage/ -pn BT -pool ${pool}

python3 code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py -i ./data/sentiment_incld/${data}/size_4_stride_2/${data}_input_jaccard_prob_span_ -sp 4 -o ./data/sentiment_incld/${data}/size_4_stride_2/graphsage/ -pn JI -pool ${pool}

done
done

