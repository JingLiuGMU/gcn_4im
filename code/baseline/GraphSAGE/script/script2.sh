#!/usr/bin/env bash

# for model in graphsage_mean graphsage_maxpool
# do
# for suffix in _BT_mean.json _BT_max.json _JI_mean.json _JI_max.json
# do


# python3 -m graphsage.supervised_train --train_prefix ../../../data/sentiment_incld/ransomware/size_5_stride_2/graphsage/ --model ${model} --sigmoid --train_suffix ${suffix} --span 3 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 1619 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2 --identity_dim 16

# done
# done

# for model in graphsage_mean graphsage_maxpool
# do
# for suffix in _LT_mean.json _LT_max.json
# do


# python3 -m graphsage.supervised_train --train_prefix ../../../data/sentiment_incld/ransomware/size_5_stride_2/graphsage/ --model ${model} --multi_logit --train_suffix ${suffix} --span 3 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 1619 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2 --identity_dim 16

# done
# done

for model in graphsage_maxpool #graphsage_mean 
do
for suffix in _LT_max.json #_LT_mean.json 
do


python3 -m graphsage.supervised_train --train_prefix ../../../data/sentiment_incld/cybersecurity/size_6_stride_1/graphsage/ --model ${model} --multi_logit --train_suffix ${suffix} --span 5 --nfeat 0 --epochs 350 --max_degree 128 --batch_size 4461 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2 --identity_dim 16

done
done

for model in graphsage_maxpool graphsage_mean 
do
for suffix in _JI_max.json _JI_mean.json # _BT_mean.json _BT_max.json  
do


python3 -m graphsage.supervised_train --train_prefix ../../../data/sentiment_incld/cybersecurity/size_6_stride_1/graphsage/ --model ${model} --sigmoid --train_suffix ${suffix} --span 5 --nfeat 0 --epochs 350 --max_degree 128 --batch_size 4461 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2 --identity_dim 16

done
done




