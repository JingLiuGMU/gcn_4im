#!/usr/bin/env bash

#for model in graphsage_mean graphsage_maxpool
#do
#for suffix in _BT_mean.json _BT_max.json _JI_mean.json _JI_max.json
#do
#
#
#python3 -m graphsage.supervised_train --train_prefix ../../../data/temporal_network/email-Eu-core-temporal/size_6_stride_4/graphsage/ --model ${model} --sigmoid --train_suffix ${suffix} --span 3 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 512 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2
#
#done
#done
#
#
#for model in graphsage_mean graphsage_maxpool
#do
#for suffix in _LT_mean.json _LT_max.json
#do
#
#
#python3 -m graphsage.supervised_train --train_prefix ../../../data/temporal_network/email-Eu-core-temporal/size_6_stride_4/graphsage/ --model ${model} --multi_logit --train_suffix ${suffix} --span 3 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 512 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2
#
#done
#done


for model in graphsage_mean graphsage_maxpool
do
for suffix in _BT_mean.json _BT_max.json _JI_mean.json _JI_max.json
do


python3 -m graphsage.supervised_train --train_prefix ../../../data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graphsage/ --model ${model} --sigmoid --train_suffix ${suffix} --span 4 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 1060 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2

done
done


for model in graphsage_mean graphsage_maxpool
do
for suffix in _LT_mean.json _LT_max.json
do


python3 -m graphsage.supervised_train --train_prefix ../../../data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graphsage/ --model ${model} --multi_logit --train_suffix ${suffix} --span 4 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 1060 --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2

done
done

