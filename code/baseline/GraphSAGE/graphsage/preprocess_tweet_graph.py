import numpy as np
import argparse
import os, json, jsonpickle
import networkx as nx
from networkx.readwrite import json_graph
import pandas as pd
import scipy.sparse as sp


def gen_input_4graphsage(args):
    if not os.path.isdir(args.output):
        os.makedirs(args.output)
    for i in range(args.span):
        print('span: {}'.format(i))
        filenamei = args.input + str(i) + '.csv'
        datai_csv = pd.read_csv(filenamei)
        label_df = datai_csv.iloc[:, [0, 1, -1]]
        label_map = label_df.set_index(['s_user', 'd_user']).T.to_dict('list')
        with open(args.output + 'label-map_period-{}_{}_{}.json'.format(i, args.prob_name, args.pool), 'w') as fout:
            json.dump({str(k): v for k, v in label_map.items()}, fout)
        np_filenamei = args.input + str(i) + '.npz'
        datai = np.load(np_filenamei)
        vfeat = init_embed_vertex(datai['fv'], datai['fe'], datai['Ae'], args.pool)
        if i==0:
            adj_v = datai['Av']
            G = nx.from_numpy_matrix(adj_v, create_using=nx.DiGraph())
            serialize(G, args.output + 'G_{}_{}.json'.format(args.prob_name, args.pool))
            id_map = dict(enumerate(np.arange(vfeat.shape[0])))
            with open(args.output + 'id-map_{}_{}.json'.format(args.prob_name, args.pool), 'w') as fout:
                json.dump({str(k): int(v) for k, v in id_map.items()}, fout)
        # with open(args.output + 'G_period-' + str(i) + '_' + args.prob_name + '.json', 'w') as fout:
        #     json.dump(G, fout)

        feat_map = dict(enumerate(vfeat.tolist()))
        with open(args.output + 'feat-map_period-{}_{}_{}.json'.format(i, args.prob_name, args.pool), 'w') as fout:
            json.dump({str(k): v for k, v in feat_map.items()}, fout)


def init_embed_vertex(fv, fe, Ae, pool):
    Ae[Ae == -1] = 0
    Ae = np.expand_dims(Ae, axis=1)

    fe = np.expand_dims(fe, axis=-1)
    feat = np.multiply(Ae, fe)

    feat = np.transpose(feat, (2, 1, 0))
    if 'max' in pool:
        feat = np.max(feat, axis=-1)
    elif 'mean' in pool:
        feat = np.mean(feat, axis=-1)
    else:
        raise TypeError('{} pool is not supported!'.format(pool))
    feat = np.squeeze(np.concatenate([fv, feat], axis=1))
    feat = preprocess_features(feat)

    # from sklearn.preprocessing import StandardScaler
    # # train_ids = np.array([id_map[n] for n in G.nodes()])
    # train_feats = feat
    # scaler = StandardScaler()
    # scaler.fit(train_feats)
    # feat = scaler.transform(feat)
    return feat
    # return


def preprocess_features(features):
    """Row-normalize feature matrix and convert to tuple representation"""

    colsum = np.array(features.sum(0))
    # import ipdb; ipdb.set_trace()
    try:
        r_inv = np.power(colsum, -1).flatten()
        r_inv[np.isinf(r_inv)] = 0.
        r_mat_inv = sp.csr_matrix(r_inv)
        features = r_mat_inv.T.multiply(features.T).T.todense()
    except Exception as e:
        print(e)
    return features


def serialize(call_graph, file_path):
    '''Function to serialize a NetworkX DiGraph to a JSON file.'''
    if not isinstance(call_graph, nx.DiGraph):
        raise Exception('call_graph has be an instance of networkx.DiGraph')

    with open(file_path, 'w+') as _file:
        _file.write(jsonpickle.encode(
            json_graph.adjacency_data(call_graph)))


def deserialize(file_path):
    '''Function to deserialize a NetworkX DiGraph from a JSON file.'''
    call_graph = None
    with open(file_path, 'r+') as _file:
        call_graph = json_graph.adjacency_graph(
            jsonpickle.decode(_file.read()),
            directed=True
        )
    return call_graph


if __name__ == '__main__':
    # G = deserialize('data/sentiment_incld/CVE/size_4_stride_2/graphsage/G_period-0_BT.json')
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', help='path to input file', dest='input', type=str,
                        default='./data/sentiment_incld/cybersecurity/size_6_stride_1/cybersecurity_input_lt_prob_span_')
    parser.add_argument('-sp', help='no. of spans', dest='span', type=int, default=5)
    parser.add_argument('-o', help='path to save output', dest='output', type=str,
                        default='./data/sentiment_incld/cybersecurity/size_6_stride_1/graphsage/')
    parser.add_argument('-pn', help='prob name', dest='prob_name', type=str, default='LT')
    parser.add_argument('-pool', help='pool for vertex init embedding', type=str, default='max')
    args = parser.parse_args()
    gen_input_4graphsage(args)
