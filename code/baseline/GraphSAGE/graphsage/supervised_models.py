import tensorflow as tf

import graphsage.models as models
import graphsage.layers as layers
from graphsage.aggregators import MeanAggregator, MaxPoolingAggregator, MeanPoolingAggregator, SeqAggregator, \
    GCNAggregator

flags = tf.app.flags
FLAGS = flags.FLAGS


class SupervisedGraphsage(models.SampleAndAggregate):
    """Implementation of supervised GraphSAGE."""

    def __init__(self, num_classes,
                 placeholders, n_features, adj, degrees,
                 layer_infos, concat=True, aggregator_type="mean",
                 model_size="small", sigmoid_loss=False, multi_logit=False, identity_dim=0,
                 **kwargs):
        '''
        Args:
            - placeholders: Stanford TensorFlow placeholder object.
            - n_features: Numpy array with node n_features.
            - adj: Numpy array with adjacency lists (padded with random re-samples)
            - degrees: Numpy array with node degrees. 
            - layer_infos: List of SAGEInfo namedtuples that describe the parameters of all 
                   the recursive layers. See SAGEInfo definition above.
            - concat: whether to concatenate during recursive iterations
            - aggregator_type: how to aggregate neighbor information
            - model_size: one of "small" and "big"
            - sigmoid_loss: Set to true if nodes can belong to multiple classes
        '''

        models.GeneralizedModel.__init__(self, **kwargs)

        if aggregator_type == "mean":
            self.aggregator_cls = MeanAggregator
        elif aggregator_type == "seq":
            self.aggregator_cls = SeqAggregator
        elif aggregator_type == "meanpool":
            self.aggregator_cls = MeanPoolingAggregator
        elif aggregator_type == "maxpool":
            self.aggregator_cls = MaxPoolingAggregator
        elif aggregator_type == "gcn":
            self.aggregator_cls = GCNAggregator
        else:
            raise Exception("Unknown aggregator: ", self.aggregator_cls)

        # get info from placeholders...
        self.inputs1 = placeholders["batch"]
        self.model_size = model_size
        self.adj_info = adj
        # self.adj_ev = adj_ev
        self.coords = placeholders['coords']
        if identity_dim > 0:
            self.embeds = tf.get_variable("node_embeddings", [adj.get_shape().as_list()[0], identity_dim])
        else:
            self.embeds = None
        if n_features <= 0:
            if identity_dim == 0:
                raise Exception(
                    "Must have a positive value for identity feature dimension if no input n_features given.")
            self.features = self.embeds
        else:
            # self.n_features = tf.Variable(tf.constant(n_features, dtype=tf.float32), trainable=False)
            self.features = placeholders['feature']
            if not self.embeds is None:
                self.features = tf.concat([self.embeds, self.features], axis=1)
        self.degrees = degrees
        self.concat = concat
        self.num_classes = num_classes
        self.sigmoid_loss = sigmoid_loss
        self.multi_logit = multi_logit
        self.dims = [(0 if n_features is None else n_features) + identity_dim]
        self.dims.extend([layer_infos[i].output_dim for i in range(len(layer_infos))])
        self.batch_size = placeholders["batch_size"]
        self.placeholders = placeholders
        self.layer_infos = layer_infos

        self.optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate)

        self.build()

    def build(self):
        samples1, support_sizes1 = self.sample(self.inputs1, self.layer_infos)
        num_samples = [layer_info.num_samples for layer_info in self.layer_infos]
        self.outputs1, self.aggregators = self.aggregate(samples1, [self.features], self.dims, num_samples,
                                                         support_sizes1, concat=self.concat, model_size=self.model_size)

        # tfprint1 = tf.Print([tf.shape(self.outputs1)], [tf.shape(self.outputs1)], 'supervised model output1 shape: ')
        # with tf.control_dependencies([tfprint1]):
        self.outputs1 = tf.nn.l2_normalize(self.outputs1, 1)

        dim_mult = 2 if self.concat else 1
        self.edge_feat = self.gather_vertex_feat()

        if self.sigmoid_loss:
            num_in = 2 * dim_mult * self.dims[-1]
            edge_feat = self.edge_feat
            for i in range(2):
                num_out = num_in // 2 + 1
                fc_layer = layers.Dense(num_in, num_out, dropout=self.placeholders['dropout'],
                                                    act=tf.nn.relu)
                edge_feat = fc_layer(edge_feat)
                num_in = num_out

            self.edge_pred_layer = layers.Dense(num_in, self.num_classes,
                                                dropout=self.placeholders['dropout'],
                                                act=tf.nn.sigmoid)
            self.edge_preds = self.edge_pred_layer(edge_feat)
        elif self.multi_logit:
            self.edge_pred_layer = layers.Dense(2 * dim_mult * self.dims[-1], self.num_classes,
                                                dropout=self.placeholders['dropout'],
                                                act=lambda x: x)
            self.edge_preds = self.edge_pred_layer(self.edge_feat)

            indices_0 = tf.range(tf.shape(self.edge_feat)[0])
            indices_1 = self.placeholders['coords'][:, 1]
            indices = tf.cast(tf.transpose(tf.stack([indices_0, indices_1])), tf.int64)
            incoming_e = tf.sparse_tensor_to_dense(tf.SparseTensor(
                indices=indices, values=tf.ones(tf.shape(self.edge_preds)[0]),
                dense_shape=[tf.shape(self.edge_preds)[0], tf.shape(self.outputs1)[0]]))
            # tfprint1 = tf.Print([tf.shape(indices), tf.shape(incoming_e)],
            #                     [tf.shape(indices), tf.shape(incoming_e)],
            #                     'tf.shape(indices_0): tf.shape(indices_1): tf.shape(indices): , tf.shape(incoming_e): ')
            # with tf.control_dependencies([tfprint1]):
            self.edge_preds = self.multi_logit_pred(incoming_e, self.edge_preds)

        else:
            raise Exception(
                    "Must choose either sigmoid or multi-logit.")
        # self.node_pred = layers.Dense(dim_mult * self.dims[-1], self.num_classes,
        #                               dropout=self.placeholders['dropout'],
        #                               act=lambda x: x)
        # # TF graph management
        # self.node_preds = self.node_pred(self.outputs1)

        self._loss()
        grads_and_vars = self.optimizer.compute_gradients(self.loss)
        clipped_grads_and_vars = [(tf.clip_by_value(grad, -5.0, 5.0) if grad is not None else None, var)
                                  for grad, var in grads_and_vars]
        self.grad, _ = clipped_grads_and_vars[0]
        self.opt_op = self.optimizer.apply_gradients(clipped_grads_and_vars)
        self.preds = self.edge_preds

    def _loss(self):
        # Weight decay loss
        for aggregator in self.aggregators:
            for var in aggregator.vars.values():
                self.loss += FLAGS.weight_decay * tf.nn.l2_loss(var)
        for var in self.edge_pred_layer.vars.values():
            self.loss += FLAGS.weight_decay * tf.nn.l2_loss(var)

        # classification loss
        if self.sigmoid_loss or self.multi_logit:
            # self.loss += tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
            #     logits=self.node_preds,
            #     labels=self.placeholders['labels']))
            self.loss += self.mse_cost(self.edge_preds, self.placeholders['labels'])
        else:
            self.loss += tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
                logits=self.node_preds,
                labels=self.placeholders['labels']))

        tf.summary.scalar('loss', self.loss)

    def gather_vertex_feat(self):
        source_feat = tf.gather(self.outputs1, self.placeholders['coords'][:, 0])
        dest_feat = tf.gather(self.outputs1, self.placeholders['coords'][:, 1])

        edge_feat = tf.concat([source_feat, dest_feat], axis=1, name='concat_vertex_edge_feat')
        return edge_feat

    def predict(self):
        if self.sigmoid_loss:
            return tf.nn.sigmoid(self.node_preds)
        else:
            return tf.nn.softmax(self.node_preds)

    def mse_cost(self, preds, labels, name='mse'):
        with tf.variable_scope(name):
            loss_cost = tf.losses.mean_squared_error(preds, labels)
            return loss_cost

    def multi_logit_pred(self, incoming_e, out_e):

        index = tf.argmax(incoming_e, axis=1)
        index, _ = tf.unique(index)

        adj_m = tf.gather(incoming_e, index, axis=1)
        out_e = tf.multiply(adj_m, out_e)
        out_e = tf.nn.softmax(out_e, axis=0)
        # set non-neighbor to 0
        out_e = tf.multiply(adj_m, out_e)

        # re-normalize
        out_e = tf.div_no_nan(out_e, tf.reduce_sum(out_e, axis=0))
        out_e = tf.reduce_sum(out_e, axis=1, keepdims=True, name='multi-logit-sum')
        return out_e
