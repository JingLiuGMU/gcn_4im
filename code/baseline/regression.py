import matplotlib.pylab as plt
import numpy as np
from sklearn.linear_model import LinearRegression, Ridge, Lasso, RANSACRegressor
import pandas as pd
#from .code.gcn.kdd_utils import cal_evl_metric
from sklearn.metrics import r2_score, mean_absolute_error, mean_squared_error
import argparse, csv, os
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import DotProduct, WhiteKernel
from sklearn.model_selection import RandomizedSearchCV


def baseline(args):
    args.dir += args.dataset + '/'
    args.dir += "size_" + str(args.window_size) + "_stride_" + str(args.stride) + "/"
    x_train, y_train, x_test, y_test = load_data(args.dir, args.dataset, args.span, args.prob_name)
    reg_model(x_train, y_train, x_test, y_test, args.output, args.baseline, args.alpha, args.prob_name, args, args.plot)


def load_data(dir, dataset, span, prob_name):
    x_train, y_train = pd.DataFrame(), pd.DataFrame()

    if prob_name == "BT":
        suffix = "_input_bern_prob_span_"
    elif prob_name == "JI":
        suffix = "_input_jaccard_prob_span_"
    elif prob_name == "LT":
        suffix = "_input_lt_prob_span_"
    else:
        raise NotImplementedError('prob name not implemented!')

    for i in range(span - 1):
        filename = dir + dataset + suffix + str(i) + ".csv"
        data = pd.read_csv(filename)
        x_train = x_train.append(data.iloc[:, 4:data.shape[1] - 1], ignore_index=True)
        y_train = y_train.append(pd.DataFrame(data.iloc[:, data.shape[1] - 1]), ignore_index=True)  # append along row!!

    filename = dir + dataset + suffix + str(span - 1) + ".csv"
    data = pd.read_csv(filename)
    x_test = data.iloc[:, 4:data.shape[1] - 1]
    y_test = data.iloc[:, data.shape[1] - 1]
    return x_train, y_train, x_test, y_test


def reg_model(x_train, y_train, x_test, y_test, output, baseline, alpha, prob_name, args, plot=False):
    if baseline == 'linear_regression':
        model = LinearRegression()
    elif baseline == 'lasso':
        model = Lasso(alpha)
    elif args.baseline == 'ridge':
        model = Ridge(alpha)
    elif args.baseline == 'random_forest':
        model = RandomForestRegressor()
        model.fit(x_train, y_train)
        # model, param = tunning_RF(model, x_train, y_train)
        y_predict = model.predict(x_test)
        r2, mae, mse, mape = cal_evl_metric(y_predict, y_test)
        if args.save:
            # save_metrics(prob_name, output, alpha, baseline, r2, mae, mse, mape, args, param)
            save_metrics(prob_name, output, alpha, baseline, r2, mae, mse, mape, args)
        print("r2: {} mae: {} mse:{} mape: {}".format(r2, mae, mse, mape))
        np.savetxt(args.dir + "pred_true_" + args.prob_name + "_" + args.baseline + ".csv",
                   np.c_[y_predict, y_test], delimiter=",")
        return
    elif args.baseline == 'gpr':
        kernel = DotProduct() + WhiteKernel()
        model = GaussianProcessRegressor(kernel=kernel, random_state=0)
    elif args.baseline == 'boost':
        # params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 2,
        #           'learning_rate': 0.01, 'loss': 'ls'}
        model = GradientBoostingRegressor()
        model.fit(x_train, y_train)
        # model, param = tunning_GBR(model, x_train, y_train)
        y_predict = model.predict(x_test)
        r2, mae, mse, mape = cal_evl_metric(y_predict, y_test)
        if args.save:
            # save_metrics(prob_name, output, alpha, baseline, r2, mae, mse, mape, args, param)
            save_metrics(prob_name, output, alpha, baseline, r2, mae, mse, mape, args)
        print("r2: {} mae: {} mse:{} mape: {}".format(r2, mae, mse, mape))
        return
    elif args.baseline == 'ransac':
        model = RANSACRegressor(random_state=0)
    else:
        raise NotImplementedError('Not supported!')

    model.fit(x_train, y_train)
    # print(model.score(x_test, y_test))
    # print(model.coef_)
    # print(model.intercept_)

    y_predict = model.predict(x_test)
    r2, mae, mse, mape = cal_evl_metric(y_predict, y_test)
    if args.save:
        save_metrics(prob_name, output, alpha, baseline, r2, mae, mse, mape, args)
    print("r2: {} mae: {} mse:{} mape: {}".format(r2, mae, mse, mape))
    np.savetxt(args.dir + "pred_true_" + args.prob_name + "_" + args.baseline + ".csv",
               np.c_[y_predict, y_test], delimiter=",")

    if plot:
        plot_chart(y_test, y_predict)


def plot_chart(y_test, y_predict, title="true vs. pred"):
    plt.plot(y_test, y_predict, '.')
    x = np.linspace(0.0, 1.0, num=10)
    y = x
    plt.plot(x, y)
    plt.title(title)
    plt.show()


def tunning_RF(RF_model, train_features, train_labels):
    # Number of trees in random forest
    n_estimators = [int(x) for x in np.linspace(start=200, stop=2000, num=10)]
    # Number of features to consider at every split
    max_features = ['auto', 'sqrt']
    # Maximum number of levels in tree
    max_depth = [int(x) for x in np.linspace(10, 110, num=11)]
    max_depth.append(None)
    # Minimum number of samples required to split a node
    min_samples_split = [2, 5, 10]
    # Minimum number of samples required at each leaf node
    min_samples_leaf = [1, 2, 4]
    # Method of selecting samples for training each tree
    bootstrap = [True, False]
    # Create the random grid
    random_grid = {'n_estimators': n_estimators,
                   'max_features': max_features,
                   'max_depth': max_depth,
                   'min_samples_split': min_samples_split,
                   'min_samples_leaf': min_samples_leaf,
                   'bootstrap': bootstrap}
    rf_random = RandomizedSearchCV(estimator=RF_model, param_distributions=random_grid, n_iter=100, cv=3, verbose=2,
                                   random_state=42, n_jobs=-1)
    # Fit the random search model
    rf_random.fit(train_features, train_labels)

    return rf_random.best_estimator_, rf_random.best_params_


def tunning_GBR(model, train_features, train_labels):
    param_grid = {'learning_rate': [0.1, 0.05, 0.03, 0.01], 'loss': ['ls', 'huber'], 'max_depth': [5, 7, 10],
                  'max_features': ['auto', 'sqrt', 'log2', None],
                  'min_samples_leaf': [2, 3, 5], 'n_estimators': [100, 500, 1000],
                  'random_state': [7]}
    boost_gs = RandomizedSearchCV(model, param_distributions=param_grid, cv=3, n_jobs=-1,
                                  n_iter=25)
    boost_gs.fit(train_features, train_labels)
    return boost_gs.best_estimator_, boost_gs.best_params_


def cal_evl_metric(pred, label):
    pred = np.squeeze(pred)
    r2 = r2_score(label, pred)
    mae = mean_absolute_error(label, pred)
    mse = mean_squared_error(label, pred)
    mape = mean_absolute_percentage_error(label, pred)
    return r2, mae, mse, mape


def mean_absolute_percentage_error(y_true, y_pred):
    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)
    predi = y_pred[np.where(y_true > 1e-5)]
    labeli = y_true[np.where(y_true > 1e-5)]
    return np.mean(np.abs(labeli - predi) / labeli)


def save_metrics(prob_name, output, alpha, baseline, r2, mae, mse, mape, args, params=None):
    if not os.path.isfile(output):
        writer = csv.writer(open(output, 'w+', newline=''))
        res_summary = ['dataset', 'window_size', 'stride', 'prob_name',
                       'r2', 'mae', 'mape', 'mse', 'baseline', 'alpha', 'param']
        writer.writerow(res_summary)

    writer = csv.writer(open(output, 'a', newline=''))
    res_summary = [args.dataset, args.window_size, args.stride,  prob_name,
                   r2, mae, mape, mse, baseline, alpha, params]
    writer.writerow(res_summary)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='data preparing.')
    parser.add_argument('--dir', '-dir', type=str, default='./data/', help='path to data file')
    parser.add_argument('--dataset', '-d', type=str, default='CVE', help='dataset to be processed')
    parser.add_argument('--span', '-sp', type=int, default=4, help='# of spans')
    parser.add_argument('--stride', '-s', type=int, default=2, help='stride between two time windows')
    parser.add_argument('--window_size', '-w', type=int, default=4, help='sliding window size')
    parser.add_argument('--plot', '-p', action="store_true", help='plot prediction chart')
    parser.add_argument('--baseline', '-b', type=str, default='ridge',
                        help='choose prediction baseline, linear_regression/ridge/lasso')
    parser.add_argument('--alpha', '-a', type=float, default=0.1, help='coefficient for lasso/ridge')
    parser.add_argument('--prob_name', '-pn', type=str, default='BT', help='specify the prob name')
    parser.add_argument('--output', '-o', type=str, default='./data/pred_baseline.csv', help='specify the prob name')
    parser.add_argument('--save', '-sa', action="store_true", help='save metrics')

    args = parser.parse_args()
    baseline(args)