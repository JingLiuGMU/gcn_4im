## Data preparation

codes in `code/data_prep` is to prepare data for the following three dataset: `CVE/ransomware/cybersecurity experts`. Run


    python ./code/data_prep/get_raw_input_common_edge.py --dir ./data/ \
    -d CVE -ce -s 3 -w 3 -np 1

+ `-d [str]` specify the dataset (CVE/ransomware/cybersecurity) to process
+ `-ce [switch]` whether to use common edge or not
+ `-s [int]` specify the stride between two consecutive time windows
+ `-w [int]` specify the window size
+ `-np [int]` specify the number of previous time windows used to predict influence probability in current time window.

The processed data is saved in `.npz` format with the keys of `Av` (adjacency matrix, Nv x Nv), `Ae` (Ne x Nv, `-1` for outgoing edge, `1` for incoming edge), edge_connectivity (Ne x Ne), fv (vertex features), fe (edge features), lable (actual influence probability). 

## Training
To visualize the training process using tensorboard, run the following cmd

    tensorboard --logdir="./data/logdir" --port 6060
    
To train the regression model, run the following command line.

    python ./code/gcn/main.py --max_step 8000 --learning_rate 0.005 --k_fold=False \
    --ch_num 4 --keep_r 0.2 --weight_decay 5e-4 --k 2 --gcl_linear=True --full_map=True --vertex_feat=True \
    --aggregator max_pooling --reg_model sigmoid --n_slice 1 --fc_layer_num 1 --layer_num 2 \
    --dataset CVE --filename ./data/CVE/size_4_stride_2/CVE_input_bern_prob_span_ --window_size 4 --stride 2 --spans 4 \    
    --logdir ./data/logdir/ --modeldir ./data/modeldir/ --gpuid "1" \
    --use_batch --batch_size 100 --center_num 15 --test_step 6000 --fixed_sub=True \
    --exp_name {dataset}_w{window_size}_s{stride}_lr{learning_rate}_keep{keep_r}_ch{ch_num}_l2_{weight_decay}_k{k}_gcl{layer_num}_gcl_linear{gcl_linear}_full_m_{full_map}_vfeat_{vertex_feat}_{reg_model}_{aggregator}_b{use_batch}_bsize{batch_size}_self{self_loop}

+ `--k_fold=False` specify it is for k-fold validation to select hyperparameters or training
+ `--exp_name [str]` specify the folder name for saving checkpoints

The following parameters are used to define the network architecture.

+ `--ch_num [int]` specify the channel number (i.e., the dimmension of hidden features)  
+ `--keep_r [float]` specify the keep ratio for dropout layer 
+ `--weight_decay [float]` specify the l2 regularization weight 
+ `--layer_num [int]` specify the number of GCN layers
+ `--aggregator [str]` specify aggregation function for GCN layer, k-max pooling, mean-pooling, LSTM are implemented.
+ `--k [int]` specify the neighbors number for aggregating features for k max pooling
+ `--gcl_linear=True` specify whether to use linear layer after each GCN layer
+ `--full_map=True` specify whether to use one FC layer (dim of input = dim of output) after all GCN layers
+ `--fc_layer_num [int]` specify the number of FC layers used before the final output layer, for each FC layer, the dimension of hidden feature is halved. 
+ `--vertex_feat=True` specify whether to use original vertex features for regression
+ `--reg_model [str]` specify activation function for the output layer.Sigmoid/Tanh/Multi-logit are implemented 
+ `--n_slice [int]` specify the slice number for dealing with GPU memory limits

The following parameters define the inputs.

+ `--dataset [str]` specify dataset
+ `--filename [str]` specify the input root 
+ `--window_size [int] --stride [int] --spans [int]` specify window size, stride, and total spans in dividing the total dataset span.

The following parameters are for subgraph training.

+ `--use_batch` a switch, specify whether to use subgraph training or not
+ `--batch_size [int]` specify the subgraph size
+ `--center_num [int]` specify the init-size of the subgraph
+ `--fixed_sub=True` specify whether the subgraph size is fixed or not. If not, the subgraph size is `max(batch size, size of one connected subgraph)`



## Influence maximization (IM)
### Find seeds

    python code/baseline/IMP-master/IMP_original.py \
    -i data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/graph_true_LT.txt \
    -o data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/seed-5_g-true_IC_LT.txt \
    -k 5 -m IC
    
+ `-i [str]` specify the root to the graph file. In the file, the first line defines the number of nodes and edges. Following lines define each edge with source node, destination node, and influence probability
+ `-o [str]` specify the root to save the seeds
+ `-k [int]` specify the number of seeds to be found
+ `-m [str]` specify the cascade model for find seeds

More information is referred to https://github.com/Wang-GY/IMP

### Calcuate influence on top of the true graph

    python code/baseline/IMP-master/ISE.py \
    -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/graph_true_LT.txt \
    -s /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/seed-5_g-ours_IC_LT.txt \
    -k 5 -m IC \
    -o /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/result_IM4TrueG.csv -p LT -g ours

+ `-i [str]` specify the root to the true graph file
+ `-s [str]` specify the root to the saved seed file
+ `-k [int]` specify the number of seeds to be found
+ `-m [str]` specify the cascade model for find seeds
+ `-o [str]` specify the root to save the calculated influence value
+ `-p [str], -g [str]` specify the probability type and the predicted graph name only for saving result purpose

### Robust IM

    python3 code/baseline/IMP-master/IMP.py 
    -i /home/jing/workspace/gcn4im/data/temporal_network/soc-redditHyperlinks-body/size_8_stride_8/graph/ \
    -inf 11.66405 10.91802 10.704204 10.536188 \
    -g 4 -k 5 -m IM -b 0 -im newgr \
    -sf _bern.txt -pn bern

+ `-i [str]` specify the root that saves all graph files
+ `-inf [float_list]` specify the a list of influence values of historical seeds (seeds found on graphs in previous time windows)
+ `-g [int]` specify the number of previous graphs
+ `-k [int]` specify the number of seeds to be found
+ `-m [str]` specify the cascade model for find seeds
+ `-b [int]` specify stopping criteria, more information on https://github.com/Wang-GY/IMP
+ `-im [str]` specify the IM method. newgr for new greedy
+ `-sf [str], -pn [str]` specify the probability type. `sf` for suffix of the file


## Baseline
### Regression
Run the following cmd for basic/advanced regression methods.

    python3 ./code/baseline/regression.py \
    --dir data/ --dataset CVE \
    -s 3 -w 3 -sp 3 -pn BT \
    -b linear_regression -o ./data/reg_result.csv

+ `-s [int] -w [int] -sp [int] -pn [str]` specify stride, window size, total spans, and probabililty type
+ `-b [str]` specify the regression method
+ `-o [str]` specify the root to save regression results (R^2, MAE, MAPE)

### LGCN-edge
Run the following cmd to train LGCN-edge. All parameters are the same with `Training` part. 
One additional parameter is `init_pool` which defines the initial way to combine node and edge features. `Mean/Max` are implemented.

    python ./code/baseline/LGCN/main.py --max_step 5000 --learning_rate 0.005 \
    --filename ./data/CVE/size_3_stride_3/CVE_input_jaccard_prob_span_ \
    --dataset CVE --stride 3 --window_size 3 --spans 3 --prob_name JI \
    --logdir ./data/logdir/ --modeldir ./data/model/LGCN/ \
    --exp_name {dataset}_{prob_name}_w{window_size}_s{stride} --reg_model sigmoid \
    --fc_layer_num 2 --gpuid '1' \
    --init_pool mean

### GraphSAGE-edge
First run the following cmd to generate data that satisfies the requirements of GraphSAGE.

    python code/baseline/GraphSAGE/graphsage/preprocess_tweet_graph.py \
    -i ./data/CVE/size_6_stride_1/CVE_input_lt_prob_span_ \
    -o ./data/CVE/size_6_stride_1/graphsage/ \
    -sp 5 -pn LT -pool mean 

+ `-pool [mean/max]` specify the way to combine node and edge features. It is the same with `init_pool` for `LGCN-edge`.

Then run the following cmd for training GraphSAGE-edge. Please refer https://github.com/williamleif/GraphSAGE for detailed explanation/

    python -m graphsage.supervised_train \
    --train_prefix ../../../data/CVE/size_6_stride_2/graphsage/ \
    --model graphsage_maxpool --sigmoid --train_suffix _LT_max.json \
    --span 3 --nfeat 0 --epochs 500 --max_degree 128 --batch_size 512 \
    --gpu 1 --print_every 100 --learning_rate 0.001 --dropout 0.2